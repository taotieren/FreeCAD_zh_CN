<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>BIMSetupDialog</name>
    <message>
        <location filename="../dialogSetup.ui" line="17"/>
        <source>BIM Setup</source>
        <translation>BIM 设置</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="23"/>
        <source>This dialog will help you to set FreeCAD up for efficient BIM workflow, by setting a couple of typical FreeCAD options. This dialog can be accessed again anytime from menu Manage -&gt; Setup, and more options are available under menu Edit -&gt; Preferences.</source>
        <translation>此对话框将设置一些典型的 FreeCAD 选项，以帮助您建立高效的 BIM 工作流。这个对话框可以在管理 -&gt; 设置菜单中再次访问，更多选项可以在编辑 -&gt; 偏好菜单中找到。</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="33"/>
        <source>Hover your mouse on each setting for additional info.</source>
        <translation>在每个设置上悬停鼠标，以获得更多信息。</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="57"/>
        <source>Number of backup files</source>
        <translation>备份文件数</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="64"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Default line width. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Display &amp;gt; Part colors &amp;gt; Default line width, Draft &amp;gt; Visual settings &amp;gt; Default line width&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;默认线宽度。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;显示 &amp;gt; 部件颜色 &amp;gt; 默认线宽度，底图 &amp;gt; 视觉设置 &amp;gt; 默认线宽度&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="67"/>
        <source> px</source>
        <translation> px</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="77"/>
        <source>Defaut font</source>
        <translation>默认字体</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="84"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Where the grid appears at FreeCAD startup. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; General settings &amp;gt; Default working plane&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;FreeCAD 启动时网格的位置。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;底图 &amp;gt; 通用设置 &amp;gt; 默认工作平面&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="88"/>
        <source>Auto (continuously adapts to the current view)</source>
        <translation>自动（不断适应当前视图）</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="93"/>
        <source>Top (XY)</source>
        <translation>上 (XY)</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="98"/>
        <source>Front (XZ)</source>
        <translation>前 (XZ)</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="103"/>
        <source>Side (YZ)</source>
        <translation>侧面 (YZ)</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="111"/>
        <source>Default grid position</source>
        <translation>默认网格位置</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="118"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of decimals you wish to see used in the interface controls and measurements. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Units &amp;gt; Number of decimals&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您看到的界面控件和测量使用的小数位数。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 单位 &amp;gt; 小数位数&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="128"/>
        <source>Default dimension style</source>
        <translation>默认尺寸样式</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="135"/>
        <source>Default workbench</source>
        <translation>默认工作台</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="142"/>
        <source>Number of decimals</source>
        <translation>小数位数</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="149"/>
        <source>Open a new document at startup</source>
        <translation>启动时打开新文档</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="156"/>
        <source>Default line width</source>
        <translation>默认线宽</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="163"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Your name (optional). You can also add your email like this: John Doe &amp;lt;john@doe.com&amp;gt;. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Document &amp;gt; Author name&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;你的名字（可选）。您也可以像这样添加您的电子邮件：John Doe &amp;lt;john@doe.com&amp;gt;。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 文档 &amp;gt; 作者名称&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="173"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Default font. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; Texts and dimensions &amp;gt; Font family, TechDraw &amp;gt; TechDraw 1 &amp;gt; Label Font&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;默认字体。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;底图 &amp;gt; 文本和尺寸 &amp;gt; 字体, TechDraw &amp;gt; TechDraw 1 &amp;gt; 标签字体&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="180"/>
        <source>Geometry color</source>
        <translation>几何颜色</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="187"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Default dimension style. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; Texts and dimensions &amp;gt; Arrow style, TechDraw &amp;gt; TechDraw 2 &amp;gt; Arrow Style&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;默认尺寸样式。在设置中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;底图 &amp;gt; 文本和尺寸 &amp;gt; 箭头样式, TechDraw &amp;gt; TechDraw 2 &amp;gt; 箭头样式&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="191"/>
        <source>dot</source>
        <translation>点</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="196"/>
        <source>arrow</source>
        <translation>箭头</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="201"/>
        <source>slash</source>
        <translation>斜杠</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="206"/>
        <source>thick slash</source>
        <translation>厚斜杠</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="214"/>
        <source>Default author for new files</source>
        <translation>新文件的默认作者</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="221"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;How many small squares between each main line of the grid. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; Grid and snapping &amp;gt; Main line every&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;;在网格每条主线之间小方形的数量。在设置中的位置：&lt;span style=&quot; font-weight:600;&quot;&gt;底图 &amp;gt; 网格和吸附 &amp;gt; 每条主线&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="224"/>
        <source> square(s)</source>
        <translation> 个正方形</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="234"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The number of backup files to keep when saving a file. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Document &amp;gt; Maximum number of backup files&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;保存文件的备份数量。设置中的位置：&lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 文档 &amp;gt; 备份文件的最大数量&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="244"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Optional license you wish to use for new files. Keep &amp;quot;All rights reserved&amp;quot; if you don&apos;t wish to use any particular license. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Document &amp;gt; Default license&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您希望新建文件使用的版权许可证（可选）。如果您不想使用任何许可证请选择 &amp;quot;All rights reserved&amp;quot; 。在设置中的位置：&lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 文档 &amp;gt; 默认许可证&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="248"/>
        <source>All rights reserved (no specific license)</source>
        <translation>版权所有 (没有特定许可)</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="253"/>
        <source>CC-BY</source>
        <translation>CC-BY</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="258"/>
        <source>CC-BY-SA</source>
        <translation>CC-BY-SA</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="263"/>
        <source>CC-BY-NC</source>
        <translation>CC-BY-NC</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="268"/>
        <source>CC-BY-SA-NC</source>
        <translation>CC-BY-SA-NC</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="281"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The unit you prefer to work with, that will be used everywhere: in dialogs, measurements and dimensions. However, you can enter any other unit anytime. For example, if you configured FreeCAD to work in millimeters, you can still enter measures as &amp;quot;10m&amp;quot; or &amp;quot;5ft&amp;quot;. You can also change the working unit anytime without causing any modification to your model. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Units &amp;gt; User system&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您喜欢使用的单位，它将在任何地方使用：在对话框、测量和尺寸标注中。但是，您可以随时输入任何其他单位。例如，如果将 FreeCAD 配置为以毫米为单位工作，则仍可以将 MEASURES 输入为 &amp;quot;10m&amp;quot; or &amp;quot;5ft&amp;quot;.您还可以随时更改工作单位，而不会对模型进行任何修改。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 单位 &amp;gt; 用户系统&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="285"/>
        <source>millimeters</source>
        <translation>毫米</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="290"/>
        <source>centimeters</source>
        <translation>厘米</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="295"/>
        <source>meters</source>
        <translation>米</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="300"/>
        <source>inches</source>
        <translation>英寸</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="305"/>
        <source>feet</source>
        <translation>英尺</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="310"/>
        <source>architectural</source>
        <translation>建筑</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="318"/>
        <source>Default license for new files</source>
        <translation>新文件的默认许可证</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="325"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the size of the smallest square of the grid. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; Grid and snapping &amp;gt; Grid spacing&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这是网格中最小正方形的大小。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;草稿 &amp;gt; 栅格和捕捉 &amp;gt; 栅格间距&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="328"/>
        <location filename="../dialogSetup.ui" line="433"/>
        <location filename="../dialogSetup.ui" line="446"/>
        <source>0 </source>
        <translation>0 </translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="340"/>
        <source>Top:</source>
        <translation>顶部：</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="347"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The top color of the 3D view background gradient. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Display &amp;gt; Colors &amp;gt; Color gradient&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;3D 视图背景渐变的顶色。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;显示 &amp;gt; 颜色 &amp;gt; 颜色渐变&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="354"/>
        <source>Bottom:</source>
        <translation>底部：</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="361"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The bottom color of the 3D view background gradient. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Display &amp;gt; Colors &amp;gt; Color gradient&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;3D 视图背景渐变的底色。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;显示 &amp;gt; 颜色 &amp;gt; 颜色渐变&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="372"/>
        <source>Lines:</source>
        <translation>线:</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="379"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default color of lines in the 3D view. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Display &amp;gt; Part Colors &amp;gt; Default line color, Draft &amp;gt; Visual settings &amp;gt; Default line color&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;3D 视图中线条的默认颜色。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;显示 &amp;gt; 零件颜色 &amp;gt; 默认线条颜色，草稿 &amp;gt; 视觉设置 &amp;gt; 默认线条颜色&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="386"/>
        <source>Faces:</source>
        <translation>面:</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="393"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default color of faces in the 3D view. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Disply &amp;gt; Part Color &amp;gt; Default shape color&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;3D 视图中面的默认颜色。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;显示 &amp;gt; 零件颜色 &amp;gt; 默认形状颜色&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="402"/>
        <source>Preferred working units</source>
        <translation>首选工作单位</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="409"/>
        <source>3D view background color</source>
        <translation>3D 视图背景颜色</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="416"/>
        <source>Default size of a grid square</source>
        <translation>网格正方形的默认大小</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="423"/>
        <source>Main grid line every</source>
        <translation>主网格线间隔</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="430"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Default dimension arrow size. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;TechDraw &amp;gt; TechDraw 2 &amp;gt; Arrow size, Draft &amp;gt; Texts and dimensions &amp;gt; Arrow size&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;默认尺寸标注箭头大小。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;TechDraw &amp;gt; TechDraw 2 &amp;gt; 箭头大小，草稿 &amp;gt; 文字和尺寸 &amp;gt; 箭头大小&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="443"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default size of texts and dimension texts. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; Texts and dimensions &amp;gt; Font size, Techdraw &amp;gt; Techdraw 2 &amp;gt; Font size&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;文本和标注文本的默认大小。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;草稿 &amp;gt; 文字和尺寸 &amp;gt; 字体大小， Techdraw &amp;gt; Techdraw 2 &amp;gt; 字体大小&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="456"/>
        <source>Default text size</source>
        <translation>默认文本大小</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="463"/>
        <source>Default dimension arrow size</source>
        <translation>默认尺寸箭头大小</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="472"/>
        <source>Helpers:</source>
        <translation>帮助：</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="479"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default color for helper objects such as grids and axes. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Arch  &amp;gt; Defaults &amp;gt; Helpers&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;辅助对象（例如网格和轴）的默认颜色。 首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;结构  &amp;gt; 默认 &amp;gt; 帮助&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="486"/>
        <source>Construction:</source>
        <translation>建筑：</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="493"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The default color of construction geometry. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;Draft &amp;gt; General Settings &amp;gt; Construction geometry color&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;构造几何体的默认颜色。 首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;草稿 &amp;gt; 通用设置 &amp;gt; 构造几何图形颜色&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="502"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Check this to make FreeCAD start with a new blank document. Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Document &amp;gt; Create new documentat startup&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;选中此选项可使 FreeCAD 从新的空白文档开始。首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 文档 &amp;gt; 在启动时创建新文档&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="512"/>
        <source>Fill with default values</source>
        <translation>用默认值填充</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="519"/>
        <source>Choose one of the presets in this list to fill all the settings below with predetermined values. Then, adjust to your likings</source>
        <translation>选择此列表中的一个预设以使用预定值填充下面的所有设置。然后，根据你的喜好进行调整</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="523"/>
        <source>Choose...</source>
        <translation>选择...</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="528"/>
        <source>Centimeters</source>
        <translation>厘米</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="533"/>
        <source>Meters</source>
        <translation>米</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="538"/>
        <source>US (imperial)</source>
        <translation>美国 (英制)</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="546"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This is the default workbench you will start with when launching FreeCAD. Options:&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;No changes&lt;/span&gt;: Everything stays the way it is now&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;BIM workbench&lt;/span&gt;: FreeCAD will start directly in the BIM workbench&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;Start workbench, BIM afterwards&lt;/span&gt;: FreeCAD will still show the Start page, but will switch automatically to the BIM workbench when opening or creating a file&lt;/p&gt;&lt;p&gt;Location in preferences: &lt;span style=&quot; font-weight:600;&quot;&gt;General &amp;gt; Auto load module&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这是启动 FreeCAD 时将使用的默认工作台。 选项：&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;无更改&lt;/span&gt;: 一切都保持现状&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;BIM 工作台&lt;/span&gt;: FreeCAD 将直接在 BIM 工作台中启动&lt;/p&gt;&lt;p&gt;- &lt;span style=&quot; font-weight:600;&quot;&gt;启动工作台，之后执行 BIM&lt;/span&gt;: FreeCAD 仍将显示起始页，但在打开或创建文件时将自动切换到 BIM 工作台&lt;/p&gt;&lt;p&gt;首选项中的位置： &lt;span style=&quot; font-weight:600;&quot;&gt;通用 &amp;gt; 自动加载模块&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="550"/>
        <source>No change</source>
        <translation>无更改</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="555"/>
        <source>BIM workbench</source>
        <translation>BIM 工作台</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="560"/>
        <source>Start workbench, switch to BIM afterwards</source>
        <translation>开始工作台，然后切换到 BIM</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="574"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Tip&lt;/span&gt;: You might also want to set the appropriate snapping modes on the Snappng toolbar. Enabling only the snap positions that you need will make drawing in FreeCAD considerably faster.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;小贴士&lt;/span&gt;: 您可能还希望在捕捉工具栏上设置适当的捕捉模式。仅启用所需的捕捉位置将使在 FreeCAD 中绘图的速度大大提高。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogSetup.ui" line="584"/>
        <source>MissingWorkbenches</source>
        <translation>工作台缺失</translation>
    </message>
</context>
<context>
    <name>Dialog</name>
    <message>
        <location filename="../dialogClassification.ui" line="14"/>
        <source>Classification manager</source>
        <translation>分类管理器</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="34"/>
        <source>Objects &amp;&amp; Materials</source>
        <translation>对象和材料</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="40"/>
        <source>Only visible objects</source>
        <translation>仅可见对象</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="52"/>
        <source>Sort by:</source>
        <translation>排序方式：</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="60"/>
        <location filename="../dialogIfcElements.ui" line="47"/>
        <location filename="../dialogIfcProperties.ui" line="47"/>
        <source>Alphabetical</source>
        <translation>按字母顺序</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="70"/>
        <location filename="../dialogIfcElements.ui" line="57"/>
        <location filename="../dialogIfcProperties.ui" line="57"/>
        <source>IFC type</source>
        <translation>IFC 类型</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="80"/>
        <location filename="../dialogIfcElements.ui" line="67"/>
        <source>Material</source>
        <translation>材质</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="85"/>
        <location filename="../dialogIfcElements.ui" line="72"/>
        <location filename="../dialogIfcProperties.ui" line="67"/>
        <source>Model structure</source>
        <translation>模型结构</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="114"/>
        <source>Object / Material</source>
        <translation>对象/材料</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="119"/>
        <source>Class</source>
        <translation>种类</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="130"/>
        <source>Available classification systems</source>
        <translation>可用的分类系统</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="136"/>
        <source>Classification systems found on this computer</source>
        <translation>计算机上找到的分类系统</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="145"/>
        <source>Search this classification system</source>
        <translation>搜索此分类系统</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="148"/>
        <source>Search...</source>
        <translation>搜索...</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="183"/>
        <source>Apply the selected class to selected materials</source>
        <translation>将选中的类应用到所选材料</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="186"/>
        <source>&lt;&lt; Apply to selected</source>
        <translation>&lt;&lt; 应用到所选</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="193"/>
        <source>Use this class as material name</source>
        <translation>使用此类为材料名称</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="196"/>
        <source>&lt;&lt; Set as name</source>
        <translation>&lt;&lt; 设置为名称</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="205"/>
        <source>Prefix with class name when applying</source>
        <translation>应用时带有分类名称的前缀</translation>
    </message>
    <message>
        <location filename="../dialogClassification.ui" line="220"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;XML files of several classification systems can be downloaded from &lt;a href=&quot;http://www.graphisoft.com/downloads/archicad/BIM_Data.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.graphisoft.com/downloads/archicad/BIM_Data.html&lt;/span&gt;&lt;/a&gt; and placed in %s&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;可以从下载多个分类系统的 XML 文件 &lt;a href=&quot;http://www.graphisoft.com/downloads/archicad/BIM_Data.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://www.graphisoft.com/downloads/archicad/BIM_Data.html&lt;/span&gt;&lt;/a&gt; 并放置在 %s&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="14"/>
        <source>IFC Elements Manager</source>
        <translation>IFC 元素管理器</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This dialog lets you change the IFC type and material associated with any BIM object in this document. Double-click the IFC type to change, or use the drop-down menu below the list.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此对话框允许您更改与此文档中的任何 BIM 对象关联的 IFC 类型和材质。双击要更改的 IFC 类型，或使用列表下方的下拉菜单。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="30"/>
        <location filename="../dialogIfcProperties.ui" line="30"/>
        <location filename="../dialogIfcQuantities.ui" line="30"/>
        <source>only visible BIM objects</source>
        <translation>仅可见的 BIM 对象</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="39"/>
        <source>order by:</source>
        <translation>排序方式：</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="103"/>
        <source>change type to:</source>
        <translation>更改类型为：</translation>
    </message>
    <message>
        <location filename="../dialogIfcElements.ui" line="110"/>
        <source>change material to:</source>
        <translation>将材料更改为：</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="14"/>
        <source>IFC Properties Manager</source>
        <translation>IFC 属性管理器</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This dialog allows you to display and manage IFC properties attached to BIM objects. Only properties and sets present in all selected objects will be displayed and editable.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;此对话框允许您显示和管理附着到 BIM 对象的 IFC 特性。将仅显示所有选定对象中存在的属性和集，并且这些属性和集是可编辑的。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="39"/>
        <source>Order by:</source>
        <translation>排序方式：</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="89"/>
        <source>Search for a property or property set:</source>
        <translation>搜索属性或属性集：</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="109"/>
        <source>Only show matches</source>
        <translation>仅显示匹配项</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="119"/>
        <source>Select All</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="128"/>
        <source>List of IFC properties for the selected objects. Double-click to edit, drag and drop to reorganize</source>
        <translation>选定对象的 IFC 特性列表。双击可编辑，拖放可重新组织</translation>
    </message>
    <message>
        <location filename="../dialogIfcProperties.ui" line="149"/>
        <source>Delete selected property/set</source>
        <translation>删除选定属性/集</translation>
    </message>
    <message>
        <location filename="../dialogIfcQuantities.ui" line="14"/>
        <source>IFC Quantities Manager</source>
        <translation>IFC 数量管理器</translation>
    </message>
    <message>
        <location filename="../dialogIfcQuantities.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Checked quantities will be exported to IFC. Quantities marked with a warning sign indicate a zero value that you might need to check. Clicking a column header will apply to all selected items.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Warning&lt;/span&gt;: Horizontal area is the area obtained when projecting the object on the ground (X,Y) plane, but vertical area is the sum of all areas of the faces that are vertical (orthogonal to the ground plane), so a wall will have its both faces counted.&lt;/p&gt;&lt;p&gt;Length, width and height values can be changed here, but beware, it might change the geometry!&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;选中的数量将输出到 IFC。标有警告符号的数量表示您可能需要检查的零值。单击列标题将应用于所有选定项目。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;警告&lt;/span&gt;: 水平面积是将对象投影到地面 (X，Y) 平面时获得的面积，但垂直面积是垂直(与地面正交)的所有面的总和，因此墙的两个面都将被计算在内。&lt;/p&gt;&lt;p&gt;长度、宽度和高度值可以在这里更改，但要注意，它可能会更改几何体！&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogIfcQuantities.ui" line="59"/>
        <source>Select all</source>
        <translation>全选</translation>
    </message>
    <message>
        <location filename="../dialogListWidget.ui" line="14"/>
        <location filename="../dialogQuantitySurveying.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="../dialogMaterialChooser.ui" line="14"/>
        <source>Choose a material</source>
        <translation>选择一种材料</translation>
    </message>
    <message>
        <location filename="../dialogNudgeValue.ui" line="14"/>
        <source>Nudge</source>
        <translation>微调</translation>
    </message>
    <message>
        <location filename="../dialogNudgeValue.ui" line="20"/>
        <source>ew nudge value:</source>
        <translation>ew 微调值：</translation>
    </message>
    <message>
        <location filename="../dialogPreflightResults.ui" line="14"/>
        <source>Test results</source>
        <translation>测试结果</translation>
    </message>
    <message>
        <location filename="../dialogPreflightResults.ui" line="43"/>
        <source>Results of test:</source>
        <translation>测试结果：</translation>
    </message>
    <message>
        <location filename="../dialogPreflightResults.ui" line="91"/>
        <source>to Report panel</source>
        <translation>到报告面板</translation>
    </message>
    <message>
        <location filename="../dialogPreflightResults.ui" line="103"/>
        <location filename="../dialogProject.ui" line="564"/>
        <location filename="../dialogQuantitySurveying.ui" line="26"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="14"/>
        <source>BIM Project Setup</source>
        <translation>BIM 项目设置</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="20"/>
        <source>This screen allows you to configure a new BIM project in FreeCAD.</source>
        <translation>此屏幕允许您在 FreeCAD 中配置新的 BIM 项目。</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="31"/>
        <source>Use preset...</source>
        <translation>使用预设...</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="54"/>
        <source>Create new document</source>
        <translation>创建新文档</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="66"/>
        <source>Project name</source>
        <translation>项目名称</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="73"/>
        <source>Unnamed</source>
        <translation>未命名</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="83"/>
        <source>If this is checked, a human figure will be added, which helps greatly to give a sense of scale when viewing the model</source>
        <translation>如果选中此选项，将添加人物，这对查看模型时提供比例感有很大帮助</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="86"/>
        <source>Add a human figure</source>
        <translation>添加人类外形</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="96"/>
        <source>Create Site</source>
        <translation>创建场地</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="108"/>
        <source>The site object contains all the data relative to the project location. Later on, you can attach a physical object representing the terrain.</source>
        <translation>站点对象包含与项目位置相关的所有数据。 稍后，您可以附加一个代表地形的物理对象。</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="120"/>
        <source> E</source>
        <translation> E</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="133"/>
        <source>Elevation</source>
        <translation>海拔</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="140"/>
        <source>Declination</source>
        <translation>偏角</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="147"/>
        <source>Default Site</source>
        <translation>默认场所</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="154"/>
        <location filename="../dialogProject.ui" line="255"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="161"/>
        <source> °</source>
        <translation> °</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="171"/>
        <source>Longitude</source>
        <translation>纬度</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="188"/>
        <source>Address</source>
        <translation>地址</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="195"/>
        <source>Latitude</source>
        <translation>经度</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="202"/>
        <source> N</source>
        <translation> N</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="220"/>
        <source>Create Building</source>
        <translation>创建建筑物</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="229"/>
        <source>This will configure a single building for this project. If your project is made of several buildings, you can duplicate it after creation and update its properties.</source>
        <translation>这将为此项目配置一栋建筑物。 如果您的项目是由几座建筑物组成的，则可以在创建后复制它并更新其属性。</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="241"/>
        <source>Gross building length</source>
        <translation>建筑总长度</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="248"/>
        <source>Gross building width</source>
        <translation>建筑总宽度</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="262"/>
        <source>Default Building</source>
        <translation>默认建筑</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="269"/>
        <source>Number of H axes</source>
        <translation>H 轴数</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="276"/>
        <source>Distance between H axes</source>
        <translation>H 轴间距</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="283"/>
        <source>Number of V axes</source>
        <translation>V 轴数</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="306"/>
        <source>Distance between V axes</source>
        <translation>V 轴间距</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="323"/>
        <source>Main use</source>
        <translation>主要用途</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="337"/>
        <location filename="../dialogProject.ui" line="354"/>
        <location filename="../dialogProject.ui" line="364"/>
        <location filename="../dialogProject.ui" line="374"/>
        <location filename="../dialogProject.ui" line="439"/>
        <source>0 </source>
        <translation>0 </translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="384"/>
        <source>Axes line width</source>
        <translation>轴线宽度</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="402"/>
        <source>Axes color</source>
        <translation>轴颜色</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="414"/>
        <source>Levels</source>
        <translation>层</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="432"/>
        <source>Level height</source>
        <translation>层高</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="449"/>
        <source>Number of levels</source>
        <translation>层数</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="458"/>
        <source>Bind levels to vertical axes</source>
        <translation>将层绑定到垂直轴</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="465"/>
        <source>Define a working plane for each level</source>
        <translation>为每层定义一个工作平面</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="472"/>
        <source>Default groups to be added to each level</source>
        <translation>要添加到每个层的默认组</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="494"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="506"/>
        <source>Del</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="527"/>
        <source>The above settings can be saved as a preset. Presets are stored as .txt files in your FreeCAD user folder</source>
        <translation>上述设置可以作为预设保存。预设在 FreeCAD 用户文件夹中以 .txt 文件存储</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="539"/>
        <source>Save preset</source>
        <translation>保存预设</translation>
    </message>
    <message>
        <location filename="../dialogProject.ui" line="576"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="62"/>
        <source>This screen lists all the components of the current document. You can select them to create a FreeCAD spreadsheet containing information from them.</source>
        <translation>此屏幕列出当前文档的所有组件。您可以选择它们来创建包含其中信息的 FreeCAD 电子表格。</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="78"/>
        <source>This dialogue window will help you to generate list of components, dimensions, materials from a opened BIM file for Quantity Surveyor purposes.</source>
        <translation>此对话框窗口将帮助您从打开的 BIM 文件生成元件、尺寸和材料的列表，以供数量测量员使用。</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="94"/>
        <source>Select from these options the values you want from each component. FreeCAD will generate a line in the spreadsheet with these values (if they are present).</source>
        <translation>从这些选项中选择要从每个组件中选择的值。FreeCAD 将使用这些值(如果存在)在电子表格中生成一行。</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="110"/>
        <source>object.Length</source>
        <translation>对象.长度</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="123"/>
        <source>Shape.Volume</source>
        <translation>形状.体积</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="136"/>
        <source>object.Label</source>
        <translation>对象.标签</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="149"/>
        <source>count</source>
        <translation>统计</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="175"/>
        <source>Select these components from the list if you want to hide the rest of them and move to Survey mode.</source>
        <translation>如果要隐藏其余部分，然后移至“测量”模式，请从列表中选择这些组件。</translation>
    </message>
    <message>
        <location filename="../dialogQuantitySurveying.ui" line="217"/>
        <source>Select these components from the list if you want to hide the rest of them and move to schedule definition mode.</source>
        <translation>如果要隐藏其余组件，然后移至计划定义模式，请从列表中选择这些组件。</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="14"/>
        <source>Spaces manager</source>
        <translation>空间管理器</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="20"/>
        <source>This screen will allow you to check the spaces configuration of your project and change some attributes.</source>
        <translation>该屏幕将允许您检查项目的空间配置并更改一些属性。</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="37"/>
        <source>Space</source>
        <translation>空间</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="42"/>
        <location filename="../dialogSpaces.ui" line="121"/>
        <source>Color</source>
        <translation>颜色</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="47"/>
        <location filename="../dialogSpaces.ui" line="61"/>
        <location filename="../dialogSpaces.ui" line="128"/>
        <source>Area</source>
        <translation>面积</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="55"/>
        <source>Total</source>
        <translation>总计</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="68"/>
        <location filename="../dialogSpaces.ui" line="135"/>
        <source>Occupants</source>
        <translation>占用</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="75"/>
        <location filename="../dialogSpaces.ui" line="145"/>
        <source>1.00 m²</source>
        <translation>1.00 m²</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="85"/>
        <location filename="../dialogSpaces.ui" line="169"/>
        <source>Electric consumption</source>
        <translation>电力消耗</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="92"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="102"/>
        <source>0 W</source>
        <translation>0 W</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="115"/>
        <source>Space information</source>
        <translation>空间信息</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="155"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="179"/>
        <source>Level</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="186"/>
        <source>Levelname</source>
        <translation>水平名称</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="205"/>
        <source> W</source>
        <translation> W</translation>
    </message>
    <message>
        <location filename="../dialogSpaces.ui" line="212"/>
        <source>Use</source>
        <translation>使用</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="14"/>
        <source>Welcome</source>
        <translation>欢迎</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="38"/>
        <source>Welcome to the BIM workbench!</source>
        <translation>欢迎来到 BIM 工作台！</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="45"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;This appears to be the first time that you are using the BIM workbench. If you press OK, the next screen will propose you to set a couple of typical FreeCAD options that are suitable for BIM work. You can change these options anytime later under menu &lt;span style=&quot; font-weight:600;&quot;&gt;Manage -&amp;gt; Setup&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;这似乎是您第一次使用 BIM 工作台。如果您按下确认，下一个屏幕将建议您设置几个适用于 BIM工 作的典型 FreeCAD 选项。您可以稍后在菜单下随时更改这些选项 &lt;span style=&quot; font-weight:600;&quot;&gt;管理  -&amp;gt; 设置&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="60"/>
        <source>How to get started?</source>
        <translation>如何开始？</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="67"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;FreeCAD is a complex application. If this is your first contact with FreeCAD, or you have never worked with 3D or BIM before, you might want to take our &lt;a href=&quot;https://www.freecadweb.org/wiki/BIM_Start_Tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;BIM tutorial&lt;/span&gt;&lt;/a&gt; first (Also available under menu &lt;span style=&quot; font-weight:600;&quot;&gt;Help -&amp;gt; BIM Tutorial&lt;/span&gt;).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;FreeCAD 是一个复杂的应用程序。如果这是您第一次接触 FreeCAD，或者您以前从未使用过 3D 或 BIM，您可能希望使用我们的 &lt;a href=&quot;https://www.freecadweb.org/wiki/BIM_Start_Tutorial&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;BIM 教程&lt;/span&gt;&lt;/a&gt; 首先（也可以在菜单下找到 &lt;span style=&quot; font-weight:600;&quot;&gt;帮助 -&amp;gt; BIM 教程&lt;/span&gt;).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="77"/>
        <source>The BIM workbench also has a complete documentation available under the Help menu. The &quot;what&apos;s this?&quot; button will also open the help page of any tool from the toolbars.</source>
        <translation>BIM 工作台还具有“帮助”菜单下的完整文档。 “这是什么？” 按钮还将打开工具栏中任何工具的帮助页面。</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="87"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;A good way to start building a BIM model is by setting up basic characteristics of your project, under menu &lt;span style=&quot; font-weight:600;&quot;&gt;Manage -&amp;gt; Project setup&lt;/span&gt;. You can also directly configure different floor plans for your project, under menu &lt;span style=&quot; font-weight:600;&quot;&gt;Manage -&amp;gt; Levels.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;开始构建 BIM 模型的好方法是在菜单下设置项目的基本特征 &lt;span style=&quot; font-weight:600;&quot;&gt;管理 -&amp;gt; 项目设置&lt;/span&gt;. 您还可以在菜单下直接为您的项目配置不同的楼层平面图 &lt;span style=&quot; font-weight:600;&quot;&gt;管理 -&amp;gt; 水平.&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="97"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;You might also want to start from an existing floor plan or 3D model made in another application. Under menu &lt;span style=&quot; font-weight:600;&quot;&gt;File -&amp;gt; Import&lt;/span&gt;, you will find a wide range of file formats that can be imported into FreeCAD.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;您可能还想从另一个应用程序中已有的平面图或 3D 模型开始。 在菜单下 &lt;span style=&quot; font-weight:600;&quot;&gt;文件 -&amp;gt; 导入&lt;/span&gt;, 您会发现可以导入 FreeCAD 的多种文件格式。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="112"/>
        <source>Keep updated!</source>
        <translation>保持更新！</translation>
    </message>
    <message>
        <location filename="../dialogWelcome.ui" line="119"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The BIM workbench is a work-in-progress and will change quite often. Make sure you update it regularly via menu &lt;span style=&quot; font-weight:600;&quot;&gt;Tools -&amp;gt; Addon Manager&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;BIM 工作台是一个进行中的工作，并且会经常更改。 确保您通过菜单定期更新 &lt;span style=&quot; font-weight:600;&quot;工具 -&amp;gt; 插件管理器&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>Form</name>
    <message>
        <location filename="../dialogClasses.ui" line="14"/>
        <source>classManager</source>
        <translation>种类管理器</translation>
    </message>
    <message>
        <location filename="../dialogClasses.ui" line="25"/>
        <source>Class</source>
        <translation>种类</translation>
    </message>
    <message>
        <location filename="../dialogClasses.ui" line="39"/>
        <location filename="../dialogWindows.ui" line="80"/>
        <location filename="../dialogWindows.ui" line="182"/>
        <source>Material</source>
        <translation>材质</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="14"/>
        <source>Custom properties</source>
        <translation>自定义属性</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="23"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="30"/>
        <source>Can only contain alphanumerical characters and no spaces. Use CamelCase typing to define spaces automatically</source>
        <translation>只能包含字母数字字符，不能包含空格。使用 CamelCase 键入自动定义空间</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="37"/>
        <location filename="../dialogWindows.ui" line="189"/>
        <source>Description</source>
        <translation>描述</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="44"/>
        <source>A description for this property, can be in any language.</source>
        <translation>此属性的描述可以使用任何语言。</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="51"/>
        <source>The property will be hidden in the interface, and can only be modified via python script</source>
        <translation>该属性将隐藏在界面中，并且只能通过 python 脚本进行修改</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="54"/>
        <source>Hidden</source>
        <translation>隐藏</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="61"/>
        <source>The property is visible but cannot be modified by the user</source>
        <translation>该属性是可见的，但不能由用户修改</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="64"/>
        <source>Read-only</source>
        <translation>只读</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="71"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../dialogCustomProperties.ui" line="83"/>
        <location filename="../dialogLevels.ui" line="98"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="14"/>
        <source>Levels manager</source>
        <translation>水平管理器</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="20"/>
        <source>The list below contains all the levels of your project. Levels in FreeCAD don&apos;t necessarily need to be horizontal or stacked, you can change a level placement directly in each level&apos;s properties</source>
        <translation>下面的列表包含项目的所有级别。FreeCAD 中的水平不必是水平的或堆叠的，您可以直接在每个水平的属性中更改水平位置</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="30"/>
        <source>Double-clicking a level, here or in the tree view, will set the working plane (and view if configured so) to that level</source>
        <translation>双击此处或树视图中的某个水平，会将工作平面(以及视图，如果已配置)设置为该水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="65"/>
        <source>Level</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="70"/>
        <location filename="../dialogWindows.ui" line="175"/>
        <source>Height</source>
        <translation>高度</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="80"/>
        <source>Add a new level using the values entered below</source>
        <translation>使用下面输入的值添加新水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="83"/>
        <source>New</source>
        <translation>新建</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="95"/>
        <source>Deletes all selected levels</source>
        <translation>删除所有选定水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="112"/>
        <source>Level name</source>
        <translation>水平名称</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="119"/>
        <source>A name for this level</source>
        <translation>此水平的名称</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="126"/>
        <source>Level base Z coordinate (floor height)</source>
        <translation>水平基准 Z 坐标（地板高度）</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="133"/>
        <source>The base height of this level, that is, the vertical distance between this floor and the ground plane</source>
        <translation>该水平面的基准高度，即此地板与地面之间的垂直距离</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="143"/>
        <source>Level height (ceiling height above floor)</source>
        <translation>水平高度（地板上方的天花板高度）</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="150"/>
        <source>The height between the floor and the ceiling of this level</source>
        <translation>此水平的地板和天花板之间的高度</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="162"/>
        <source>Check this to make the level adopt a stored view angle when double-clicked</source>
        <translation>选中此项以使水平仪在双击时采用存储的视角</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="165"/>
        <source>Set view on double-click</source>
        <translation>双击设置视图</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="172"/>
        <source>Stores the current view angle to the selected levels</source>
        <translation>将当前视角存储到所选水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="175"/>
        <source>Use current</source>
        <translation>使用当前</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="184"/>
        <source>Check this to make this level hide all other levels when double-clicked</source>
        <translation>选中此项以使该水平在双击时隐藏所有其他水平</translation>
    </message>
    <message>
        <location filename="../dialogLevels.ui" line="187"/>
        <source>Hide all other levels on double-click</source>
        <translation>双击隐藏所有其他水平</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="14"/>
        <source>Library browser</source>
        <translation>库浏览器</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="27"/>
        <source>Insert &gt;&gt;</source>
        <translation>插入 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="36"/>
        <source>Search:</source>
        <translation>搜索:</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="48"/>
        <source>Search BimObjects.com</source>
        <translation>搜索 BimObjects.com</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="55"/>
        <source>Search NationalBimLibrary.com</source>
        <translation>搜索 NationalBimLibrary.com</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="62"/>
        <source>Search BimTool.com</source>
        <translation>搜索 BimTool.com</translation>
    </message>
    <message>
        <location filename="../dialogLibrary.ui" line="69"/>
        <source>Note: FCStd and IFC files cannot be placed. STEP and BREP files can be placed at custom location</source>
        <translation>注意：无法放置 FCStd 和 IFC 文件。 STEP 和 BREP 文件可以放置在自定义位置</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="14"/>
        <source>IFC Preflight</source>
        <translation>IFC 预检</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="20"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The following test will check your model or the selected object(s) and their children for conformity to some IFC standards.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Important&lt;/span&gt;: None of the failed tests below will prevent exporting IFC files, nor do these tests guarantee that your IFC files meets some specific quality or standard requirement. They are there to help you assess what is and what is not in your exported file. It&apos;s for you to choose which item is of importance to you or not. Hovering the mouse over each description will give you more information to decide.&lt;/p&gt;&lt;p&gt;After a test is run, clicking the corresponding button will give you more information to help you to fix problems.&lt;/p&gt;&lt;p&gt;The &lt;a href=&quot;http://www.buildingsmart-tech.org/specifications&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;official IFC website&lt;/span&gt;&lt;/a&gt; contains a lot of useful information about IFC standards.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;以下测试将检查您的模型或所选对象及其子对象是否符合某些 IFC 标准。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;重要&lt;/span&gt;: 以下所有失败的测试都不会阻止导出 IFC 文件，这些测试也不保证您的 IFC 文件满足某些特定的质量或标准要求。 它们在那里可以帮助您评估导出文件中的内容和内容。 由您选择哪个项目对您来说很重要。 将鼠标悬停在每个描述上将为您提供更多信息，供您确定。&lt;/p&gt; &lt;p&gt;运行测试后，单击相应的按钮将为您提供更多信息，以帮助您解决问题。&lt;/p&gt;&lt;p&gt;这个 &lt;a href=&quot;http://www.buildingsmart-tech.org/specifications&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;IFC 官方网站&lt;/span&gt;&lt;/a&gt; 包含许多有关 IFC 标准的有用信息。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="30"/>
        <source>Warning, this can take some time!</source>
        <translation>警告，这可能需要一段时间！</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="33"/>
        <source>Run all tests</source>
        <translation>运行所有测试</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="40"/>
        <source>Work on</source>
        <translation>从事于</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="46"/>
        <source>Selection</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="53"/>
        <source>All visible objects</source>
        <translation>所有可见对象</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="63"/>
        <source>Whole document</source>
        <translation>整个文档</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="73"/>
        <source>IFC export</source>
        <translation>IFC 导出</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="79"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC export in FreeCAD is performed by an open-source third-party library called IfcOpenShell. To be able to export to the newer IFC4 standard, IfcOpenShell must have been compiled with IFC4 support enabled.  This test will check if IFC4 support is available in your version of IfcOpenShell. If not, you will only be able to export IFC files in the older IFC2x3 standard. Note that some applications out there still have incomplete or inexistant IFC4 support, so in some cases IFC2x3 might still work better.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;FreeCAD 中的 IFC 导出由一个名为 IfcOpenShell 的开源第三方库执行。 为了能够导出到较新的 IFC4 标准，IfcOpenShell 必须已在启用 IFC4 支持的情况下进行了编译。 此测试将检查您的 IfcOpenShell 版本是否提供 IFC4 支持。 否则，您将只能以较旧的 IFC2x3 标准导出 IFC 文件。 请注意，那里的某些应用程序仍不完整或不存在 IFC4 支持，因此在某些情况下，IFC2x3 可能仍能更好地工作。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="82"/>
        <source>Is IFC4 support enabled?</source>
        <translation>是否启用了 IFC4 支持？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="92"/>
        <location filename="../dialogPreflight.ui" line="108"/>
        <location filename="../dialogPreflight.ui" line="141"/>
        <location filename="../dialogPreflight.ui" line="161"/>
        <location filename="../dialogPreflight.ui" line="181"/>
        <location filename="../dialogPreflight.ui" line="197"/>
        <location filename="../dialogPreflight.ui" line="230"/>
        <location filename="../dialogPreflight.ui" line="259"/>
        <location filename="../dialogPreflight.ui" line="266"/>
        <location filename="../dialogPreflight.ui" line="312"/>
        <location filename="../dialogPreflight.ui" line="332"/>
        <location filename="../dialogPreflight.ui" line="339"/>
        <location filename="../dialogPreflight.ui" line="355"/>
        <location filename="../dialogPreflight.ui" line="388"/>
        <location filename="../dialogPreflight.ui" line="408"/>
        <location filename="../dialogPreflight.ui" line="428"/>
        <source>Test</source>
        <translation>测试</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="102"/>
        <source>Project structure</source>
        <translation>项目结构</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="115"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;All IfcBuildingStorey (levels) elements are required to be inside an IfcBuilding element. This is a mandatory requirement of the IFC standard. When exporting your FreeCAD model to IFC, a default IfcBuilding will be created for all level objects (BuildingPart objects with their IFC role set as Building Storey)  found that are not inside a Building. However, it is best if you create that building yourself, so you have more control over its name and properties. This test is here to help you to find those levels without buildings.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;所有 IfcBuildingStorey（级别）元素都必须位于 IfcBuilding 元素内。 这是 IFC 标准的强制性要求。 将 FreeCAD 模型导出到 IFC 时，将为不在建筑物内的所有级别对象（其 IFC 角色设置为 Building Storey 的 Buildinging 对象）创建默认的 IfcBuilding。 但是，最好是自己创建该建筑物，这样您就可以更好地控制其名称和属性。 此测试可帮助您找到没有建筑物的那些楼层。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="118"/>
        <source>Are all storeys part of a building?</source>
        <translation>所有的楼层都是建筑物的一部分吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="128"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;All elements derived from IfcProduct (that is, all the BIM elements that compose your model)  are required to be inside an IfcBuildingStorey (level) element. This is a mandatory requirement of the IFC standard. When exporting your FreeCAD model to IFC, a default IfcBuildingStorey will be created for all BIM objects found that are not inside one already. However, it is best if you make sure yourself that all elements are correctly located inside a level, so you have more control over it. This test is here to help you to find those BIM objects without a level.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;所有从 IfcProduct 派生的元素（即构成模型的所有 BIM 元素）都必须位于 IfcBuildingStorey（级别）元素内。 这是 IFC 标准的强制性要求。 将 FreeCAD 模型导出到 IFC 时，将为发现的所有 BIM 对象创建默认的 IfcBuildingStorey。 但是，最好确保自己所有元素都正确放置在一个水平中，以便对它有更多的控制权。 此测试是在这里帮助您查找那些没有水平的 BIM 对象。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="131"/>
        <source>Are all BIM objects part of a level?</source>
        <translation>所有 BIM 对象都属于水平吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="148"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;All IfcBuilding elements are required to be inside an IfcSite element. This is a mandatory requirement of the IFC standard. When exporting your FreeCAD model to IFC, a default IfcSite will be created for all Building objects found that are not inside a Site. However, it is best if you create that site yourself, so you have more control over its name and properties. This test is here to help you to find those buildings without sites.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;所有 IfcBuilding 元素都必须位于 IfcSite 元素内。 这是 IFC 标准的强制性要求。 将 FreeCAD 模型导出到 IFC 时，将为所有不在站点内的建筑对象创建默认的 IfcSite。 但是，最好是自己创建该场地，这样您就可以更好地控制其名称和属性。 此测试可帮助您找到没有场地的建筑物。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="151"/>
        <source>Are all buildings part of a site?</source>
        <translation>所有的建筑都是场地的一部分吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="168"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The IFC standard requires at least one site, one building and one level or building storey per project. This test will ensure that at least one object of each of these 3 types exists in the model.&lt;/p&gt;&lt;p&gt;Note that, as this is a mandatory requirement, FreeCAD will automatically add a default site, a default building and/or a default building storey if any of these is missing. So even if this test didn&apos;t pass, your exported IFC file will meet the requirements.&lt;/p&gt;&lt;p&gt;However, it is always better to create these objects yourself, as you get more control over naming and properties.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC 标准要求每个项目至少有一个场地，一栋建筑和一层或多层。 此测试将确保模型中存在这三种类型的至少一个对象。&lt;/p&gt; &lt;p&gt;请注意，由于这是强制性要求，因此 FreeCAD 将自动添加默认场地，默认建筑物和/或默认的建筑物楼层（如果缺少这些楼层）。 因此，即使该测试没有通过，您导出的 IFC 文件也将满足要求。&lt;/p&gt; &lt;p&gt;但是，最好自己创建这些对象，因为您可以更好地控制命名和属性。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="171"/>
        <source>Is there at least one site, one building and one level in the model?</source>
        <translation>模型中是否至少有一个场地，一栋建筑物和一层？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="191"/>
        <source>Geometry</source>
        <translation>几何属性</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="204"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Although it is not a requirement for IFC objects to have fully clean and solid geometry (and you will more than often find IFC files with bad geometry out there, oh boy if you find!), it is of course better if they do. You will reduce chances of problems with other applications, and after all, in real life, all objects have solid shapes.&lt;/p&gt;&lt;p&gt;FreeCAD has a lot of tools to check for geometry quality, and most parametric objects, including BIM objects, will usually warn you if their geometry becomes unclean or not solid at some point. This test makes sure everything is OK.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;尽管并不需要 IFC 对象具有完全干净且完整的几何体（而且您会比经常发现存在不良几何体的 IFC 文件，如果发现的话，天哪！），如果这样做的话，当然更好。 您将减少其他应用程序出现问题的机会，毕竟，在现实生活中，所有对象都具有实体形状。&lt;/p&gt; &lt;p&gt; FreeCAD 有很多工具可以检查几何图形质量以及大多数参数对象，包括 BIM 对象，如果它们的几何形状在某些时候变得不干净或不牢固，通常会警告您。 此测试确保一切正常。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="207"/>
        <source>Are all BIM objects solid and valid?</source>
        <translation>所有 BIM 对象是否牢固且有效？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="217"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The IFC format provides a defined type for most of the objects that compose a building, for example walls, columns, doors, or sinks. But it also supports undefined objects, which are given the generic BuildingElementProxy type. This test will check that all objects have a defined type.&lt;/p&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;p&gt;Note that failing this test is not necessarily bad, as you might specifically want some object to not have any defined type. In some cases, this might even give better results, as some applications like Revit might add possibly unwanted additional constraints or transformations to some known types such as structural elements (beams or columns). Exporting them as BuildingElementProxies will prevent that.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC 格式为构成建筑物的大多数对象（例如墙，柱，门或水槽）提供了定义的类型。 但它也支持未定义的对象，这些对象具有通用的 BuildingElementProxy 类型。 此测试将检查所有对象是否具有定义的类型。&lt;/p&gt; &lt;p&gt; &lt;br/&gt; &lt;/p&gt; &lt;p&gt;请注意，未通过此测试并不一定很糟糕，因为您可能特别希望某些对象没有 任何定义的类型。 在某些情况下，这甚至可能会带来更好的结果，因为诸如 Revit 之类的某些应用程序可能会对某些已知类型（例如结构元素（梁或柱））添加不必要的附加约束或转换。 将它们导出为 BuildingElementProxies 将防止这种情况。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="220"/>
        <source>Are all BIM objects of a defined IFC type?</source>
        <translation>所有 BIM 对象是否都已定义 IFC 类型？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="240"/>
        <source>Properties</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="246"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Classification systems, such as UniClass or MasterFormat, or even your own custom system, are in some cases an important part of a building project. This test will ensure that all BIM objects and materials found in the model have their standardcode property dutifully filled.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;分类系统，例如 UniClass 或 MasterFormat，甚至您自己的自定义系统，在某些情况下是建筑项目的重要组成部分。 该测试将确保在模型中找到的所有 BIM 对象和材料均忠实地填充了他们的 standardcode 属性。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="249"/>
        <source>Do all BIM objects and materials have a standard classification code defined?</source>
        <translation>是否所有 BIM 对象和材料都定义了标准分类代码？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="273"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The IFC standard offers standard, predefined property sets for many object types. for example, the property set Pset_WallCommon contains properties that the IFC standard thinks all walls should have. This test will check that all BIM objects have the right propery set, if availalbe.&lt;/p&gt;&lt;p&gt;Note that this is by no means a formal requirement, and these will inflate the size of your IFC file consequently. We suggest you add standard properrt sets only if you are actually using any of them.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC 标准为许多对象类型提供了标准的预定义属性集。 例如，属性集 Pset_WallCommon 包含 IFC 标准认为所有墙都应具有的属性。 此测试将检查所有 BIM 对象（如果有）是否设置了正确的属性。&lt;/p&gt; &lt;p&gt;请注意，这绝不是正式要求，因此会增加 IFC 文件的大小。 我们建议您仅在实际使用任何标准属性集时才添加它们。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="276"/>
        <source>Do all common IFC types have the corresponding Property Set?</source>
        <translation>所有常见的 IFC 类型都具有相应的属性集吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="286"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC objects have a geometry representation, which defines the shape of the object, but can also have some or their dimensions, such as height, width or area, explicitely stated. This is very useful for BIM applications that don&apos;t process the geometry, such as spreadhseets. Those applications are still able to get and estimate quantities from IFC objects without the need to analyze the geometry.&lt;/p&gt;&lt;p&gt;It is also a possibility for errors (or even fraud), as nothing guarantes that those explicitely stated dimensions match what is inside the geometry.&lt;/p&gt;&lt;p&gt;This test will find any BIM object that has available dimension properties such as width or height, for example walls and structures, but such properties are not marked for explicit export to IFC.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC 对象具有几何图形表示形式，该图形表示形式定义了对象的形状，但也可以明确表示某些尺寸或它们的尺寸，例如高度，宽度或面积。 这对于不处理几何图形的 BIM 应用程序（例如，展布图）非常有用。 这些应用程序仍然能够从 IFC 对象获取和估计数量，而无需分析几何。&lt;/p&gt; &lt;p&gt;这也有可能发生错误（甚至是欺诈），因为没有任何保证可以明确说明那些尺寸是否匹配 &lt;/p&gt; &lt;p&gt;此测试将查找具有可用尺寸属性（例如宽度或高度）的任何 BIM 对象，例如墙和结构，但未标记这些属性以明确导出到 IFC。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="289"/>
        <source>Do all geometric BIM objects have explicit dimensions set?</source>
        <translation>是否所有几何 BIM 对象都设置了明确的尺寸？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="299"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Although there is no requirement for IFC objects to have a material defined, in the real world, it is an important layer of information to be added to you model. This test will find BIM objects without a material defined.&lt;/p&gt;&lt;p&gt;If a BIM object is exported without a material, it will nevertheless be assigned an IfcSurfaceStyle, which will be created from the object color. Some BIM applications actually disregard materials, and only consider the suface style of an object. No IfcMaterial will be attributed to that object.&lt;/p&gt;&lt;p&gt;If a BIM object has a material defined, a surface style will still be created (an IfcMaterial too) but its surface style will take the same name and properties as the material, thus giving more consistency to your file, no matter what other BIM consider, surface style, materla, or both.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;尽管不需要 IFC 对象定义材料，但在现实世界中，它是要添加到模型中的重要信息层。 此测试将查找未定义材料的 BIM 对象。&lt;/p&gt; &lt;p&gt;如果导出的 BIM 对象不包含材料，则仍将为其分配一个 IfcSurfaceStyle，该颜色将由对象颜色创建。 某些 BIM 应用程序实际上忽略了材质，仅考虑对象的表面样式。 &lt;/p&gt; &lt;p&gt;如果 BIM 对象定义了材质，则仍将创建表面样式（也可以是 IfcMaterial），但是其表面样式将具有与该样式相同的名称和属性。 材质，从而使您的文件更具一致性，无论其他 BIM 考虑什么，表面样式，materla 或两者兼而有之。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="302"/>
        <source>Do all BIM objects have a material?</source>
        <translation>所有 BIM 对象是否都有材料？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="319"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Even if a BIM object has a standard property set for its type attributed, there is no guarantee that this property set still contains or only contains all the properties that the IFC standard has defined for that set. They might have been modified after the property set has been added.&lt;/p&gt;&lt;p&gt;This test will check that all standard property sets found throughout the model contain all and only the properties specified in the standard definition.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;即使 BIM 对象为其类型的属性设置了标准属性，也无法保证此属性集仍包含或仅包含 IFC 标准已为该集合定义的所有属性。 在添加属性集之后，可能已对它们进行了修改。&lt;/p&gt; &lt;p&gt;此测试将检查整个模型中发现的所有标准属性集是否仅包含标准定义中指定的所有属性。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="322"/>
        <source>Do all standard Property Set contain the correct properties?</source>
        <translation>所有标准属性集都包含正确的属性吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="349"/>
        <source>Optional/Compatibility</source>
        <translation>可选/兼容性</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="362"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The geometry of IFC objects can be defined in a large number of ways, such as extrusions, subtractions, revolutions, or even faceted objects.&lt;/p&gt;&lt;p&gt;However, extrusions of flat shapes, which is the most basic and common type, often offer advantages over other types in other BIM applications.&lt;/p&gt;&lt;p&gt;This test will find any object that cannot be exported to IFC as an extrusion, or as a shared extrusion (clone).&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;IFC 对象的几何形状可以通过多种方式定义，例如拉伸，减法，旋转甚至是多面对象。&lt;/p&gt; &lt;p&gt;但是，扁平形状的拉伸是最基本，最常见的类型 ，通常比其他 BIM 应用程序中的其他类型更具优势。&lt;/p&gt; &lt;p&gt;此测试将查找无法作为拉伸或共享拉伸（克隆）导出到 IFC 的任何对象。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="365"/>
        <source>Are all object exportable as extrusions?</source>
        <translation>所有对象都可以导出为拉伸吗？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="375"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Walls, columns and beams in FreeCAD can be constructed in a wide number of ways. But some simpler BIM applications might have difficulties with walls that are not of the most simple type, that is, a single, straight piece of wall (which correspond to the IfcWallStandardCase type) or beams and columns that are not based on a straight extrusion of a flat profile (BeamStandardCase, ColumnStandardCase)&lt;/p&gt;&lt;p&gt;This test will find any wall which is not such a standard case.&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;Note&lt;/span&gt;: At the moment, BIM objects that meet the requirements to be of a standard case, are still exported as IfcWall, IfcBeam, IfcColumn.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;FreeCAD 中的墙，柱和梁可以以多种方式构造。 但是，某些较简单的 BIM 应用程序可能会遇到非最简单类型的墙（即，单个直的墙（对应于 IfcWallStandardCase 类型）的墙或不基于直角拉伸的梁和柱的问题） 平面轮廓（BeamStandardCase，ColumnStandardCase）&lt;/p&gt; &lt;p&gt;此测试将查找不属于这种标准情况的任何墙壁。&lt;/p&gt;&lt;p&gt;&lt;span style=&quot; font-weight:600;&quot;&gt;注意 &lt;/span&gt;：目前，符合标准案例要求的 BIM 对象仍将导出为 IfcWall，IfcBeam，IfcColumn。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="378"/>
        <source>Are all walls, beams and columns based on a single line or profile (standard case)?</source>
        <translation>是否所有墙壁、梁和柱以单条线或轮廓为依据（标准情况）？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="395"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Revit discards all objects that contain lines smaller than 1/32 inch (0.8mm). This test will find any object containing lines smaller than that value.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Revit 会丢弃所有线条小于 1/32 英寸（0.8 毫米）的对象。 该测试将找到包含小于该值的行的任何对象。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="398"/>
        <source>Are all lines bigger than 1/32 inches (minimum accepted by Revit)?</source>
        <translation>所有线条是否都大于 1/32 英寸（Revit 接受的最小值）？</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="415"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;When exporting a model to IFC, all BIM objects that are an extrusion of a rectangular profile will use an IfcRectangleProfileDef entity as their extrusion profile. However, Revit won&apos;t import these correctly. If you are going to use the IFC file in Revit, we recommend you to disable this behaviour by checking the option under menu &lt;span style=&quot; font-weight:600;&quot;&gt;Edit -&amp;gt; Preferences -&amp;gt; Import/Export -&amp;gt; IFC -&amp;gt; Disable IfcRectangularProfileDef&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;When that option is checked, all extrusion profiles will be exported as generic IfcArbitraryProfileDef entities, regardless of if they are rectangular or not, which will contain a little less information, but will open correctly in Revit.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;在将模型导出到 IFC 时，所有作为矩形轮廓的拉伸的BIM对象都将使用 IfcRectangleProfileDef 实体作为其拉伸轮廓。 但是，Revit 无法正确导入这些文件。 如果要在 Revit 中使用 IFC 文件，建议您通过选中菜单下的选项来禁用此行为 &lt;span style=&quot; font-weight:600;&quot;&gt;编辑 -&amp;gt; 首选项 -&amp;gt; 导入/导出 -&amp;gt; IFC -&amp;gt; 显示 IfcRectangularProfileDef&lt;/span&gt;.&lt;/p&gt;&lt;p&gt;选中该选项后，所有拉伸配置文件都将导出为通用 IfcArbitraryProfileDef 实体，而不管它们是否为矩形，这将包含少量信息，但可以在 Revit 中正确打开。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogPreflight.ui" line="418"/>
        <source>Is IfcRectangleProfileDef export disabled? (Revit only)</source>
        <translation>是否禁用 IfcRectangleProfileDef 导出？ （仅限 Revit）</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="14"/>
        <source>BIM tutorial</source>
        <translation>BIM 教程</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="20"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Fira Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Loading tutorials contents from the FreeCAD wiki. Please wait...&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;If this is the first time you are using the tutorial, this can take a while, since we need to download many images. On next runs, this will be faster as the images are cached locally.&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;When the tutorial is fully written, we&apos;ll think of a faster system to avoid this annoying loading time. Please bear with us in the meantime! ;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Fira Sans&apos;; font-size:9pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;从 FreeCAD Wiki 加载教程内容。 请稍候...&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;如果这是您第一次使用本教程，则可能需要一段时间，因为我们需要下载许多图像。 在下一次运行时，这将更快，因为图像将在本地缓存。&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;当教程完全编写完成后，我们将考虑使用一个更快的系统来避免这种烦人的加载时间。 在此同时，请多多包涵！ ;)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="41"/>
        <source>Tasks to complete:</source>
        <translation>要完成的任务：</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="50"/>
        <source>Goal1</source>
        <translation>目标 1</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="60"/>
        <location filename="../dialogTutorial.ui" line="77"/>
        <source>icon</source>
        <translation>图标</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="70"/>
        <source>Goal2</source>
        <translation>目标 2</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="91"/>
        <source>&lt;&lt; Previous</source>
        <translation>&lt;&lt; 上一个</translation>
    </message>
    <message>
        <location filename="../dialogTutorial.ui" line="98"/>
        <source>Next &gt;&gt;</source>
        <translation>下一个 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="14"/>
        <source>Doors and windows</source>
        <translation>门和窗</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="20"/>
        <source>This screen lists all the windows of the current document. You can modify them individually or together</source>
        <translation>该屏幕列出了当前文档的所有窗口。 您可以单独或一起修改它们</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="32"/>
        <source>Group by:</source>
        <translation>分组依据：</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="40"/>
        <source>Do not group</source>
        <translation>不分组</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="50"/>
        <source>Size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="60"/>
        <source>Clone</source>
        <translation>克隆</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="70"/>
        <location filename="../dialogWindows.ui" line="151"/>
        <location filename="../dialogWindows.ui" line="196"/>
        <source>Tag</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="97"/>
        <source>Total number of doors:</source>
        <translation>门总数：</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="104"/>
        <source>Total number of windows:</source>
        <translation>窗口总数：</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="111"/>
        <location filename="../dialogWindows.ui" line="121"/>
        <source>0</source>
        <translation>0</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="161"/>
        <source>Width</source>
        <translation>宽度</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="168"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="203"/>
        <location filename="../dialogWindows.ui" line="240"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../dialogWindows.ui" line="233"/>
        <source>Spaces</source>
        <translation>空间</translation>
    </message>
</context>
<context>
    <name>Arch</name>
    <message>
        <location filename="../BimIfcProperties.py" line="341"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="341"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="341"/>
        <source>Value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="481"/>
        <source>New property</source>
        <translation>新建属性</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="522"/>
        <source>New property set</source>
        <translation>新属性集</translation>
    </message>
</context>
<context>
    <name>BIM</name>
    <message>
        <location filename="../BimBox.py" line="172"/>
        <source>Box dimensions</source>
        <translation>方框尺寸</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="34"/>
        <source>Length</source>
        <translation>长度</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="35"/>
        <source>Width</source>
        <translation>宽度</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="36"/>
        <source>Height</source>
        <translation>高度</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="37"/>
        <source>Area</source>
        <translation>面积</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="38"/>
        <source>Horizontal Area</source>
        <translation>水平区域</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="39"/>
        <source>Vertical Area</source>
        <translation>垂直区域</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="40"/>
        <source>Volume</source>
        <translation>体积</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="114"/>
        <source>Label</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="../BimClassification.py" line="72"/>
        <source>Editing</source>
        <translation>编辑</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="92"/>
        <source>Trash</source>
        <translation>垃圾</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="460"/>
        <source>Millimeters</source>
        <translation>毫米</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="461"/>
        <source>Centimeters</source>
        <translation>厘米</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="462"/>
        <source>Meters</source>
        <translation>米</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="463"/>
        <source>Inches</source>
        <translation>英寸</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="464"/>
        <source>Feet</source>
        <translation>英尺</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="465"/>
        <source>Architectural</source>
        <translation>建筑</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="469"/>
        <source>Custom...</source>
        <translation>自定义...</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="615"/>
        <source>The value of the nudge movement (rotation is always 45°).

CTRL+arrows to move
CTRL+, to rotate left
CTRL+. to rotate right
CTRL+PgUp to extend extrusion
CTRL+PgDown to shrink extrusion
CTRL+/ to switch between auto and manual mode</source>
        <translation>微调的值（旋转始终为45°）。

CTRL+移动箭头
CTRL+, 向左旋转
CTRL+. 向右旋转
CTRL+PgUp 延伸拉伸
CTRL+PgDown 收缩拉伸
CTRL+/ 要在自动和手动模式之间切换，请执行以下操作</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="644"/>
        <source>The preferred unit you are currently working with. You can still use any other unit anywhere in FreeCAD</source>
        <translation>您当前使用的首选设备。您仍然可以在 FreeCAD 中的任何位置使用任何其他单位</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="655"/>
        <source>Toggle report panels on/off (Ctrl+0)</source>
        <translation>打开/关闭报告面板 (Ctrl+0)</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="673"/>
        <source>Toggle BIM views panel on/off (Ctrl+9)</source>
        <translation>开启/关闭 BIM 视图面板 (Ctrl+9)</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="688"/>
        <source>An update to the BIM workbench is available. Click here to open the addons manager.</source>
        <translation>BIM 工作台已有更新。 单击此处打开插件管理器。</translation>
    </message>
    <message>
        <location filename="../BimNudge.py" line="136"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="63"/>
        <source>The document currently viewed must be your main one. The other contains newer objects that you wish to merge into this one. Make sure only the objects you wish to compare are visible in both. Proceed?</source>
        <translation>当前查看的文档必须是您的主要文档。 另一个包含希望合并到此对象中的较新对象。 确保只有要比较的对象在两个对象中均可见。 继续？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="244"/>
        <source>objects still have the same shape but have a different material. Do you wish to update them in the main document?</source>
        <translation>物体仍具有相同的形状，但材质不同。 您要在主文档中更新它们吗？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="271"/>
        <source>objects have no IFC ID in the main document, but an identical object with an ID exists in the new document. Transfer these IDs to the original objects?</source>
        <translation>对象在主文档中没有 IFC ID，但是新文档中存在具有 ID 的相同对象。 将这些 ID 转移到原始对象？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="282"/>
        <source>objects had their name changed. Rename them?</source>
        <translation>对象的名称已更改。 重命名？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="291"/>
        <source>objects had their properties changed. Update?</source>
        <translation>对象的属性已更改。 更新？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="299"/>
        <source>objects have their location changed. Move them to their new position?</source>
        <translation>对象的位置已更改。 将它们移到新位置？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="307"/>
        <source>Do you wish to colorize the objects that have moved in yellow in the other file (to serve as a diff)?</source>
        <translation>您是否希望为另一个文件中已移动为黄色的对象着色（用作差异）？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="316"/>
        <source>Do you wish to colorize the objects that have been modified in orange in the other file (to serve as a diff)?</source>
        <translation>您是否希望为另一个文件中的橙色修改过的对象着色（用作差异）？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="325"/>
        <source>objects don&apos;t exist anymore in the new document. Move them to a &apos;To Delete&apos; group?</source>
        <translation>新文档中不再存在对象。 将它们移到“删除”组？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="331"/>
        <source>Do you wish to colorize the objects that have been removed in red in the other file (to serve as a diff)?</source>
        <translation>您是否希望为另一个文件中已被红色删除的对象着色（用作差异）？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="341"/>
        <source>Do you wish to colorize the objects that have been added in green in the other file (to serve as a diff)?</source>
        <translation>您是否希望为另一个文件中的绿色添加的对象上色（用作差异）？</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="350"/>
        <source>You need two documents open to run this tool. One which is your main document, and one that contains new objects that you wish to compare against the existing one. Make sure only the objects you wish to compare in both documents are visible.</source>
        <translation>您需要打开两个文档才能运行此工具。 一个是您的主要文档，另一个包含您希望与现有对象进行比较的新对象。 确保只有您希望在两个文档中进行比较的对象才可见。</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="73"/>
        <source>Select material</source>
        <translation>选择材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="93"/>
        <source>Search...</source>
        <translation>搜索...</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="94"/>
        <source>Searches object labels</source>
        <translation>搜索对象标签</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="108"/>
        <source>Clears the search field</source>
        <translation>清除搜索栏</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="116"/>
        <source>Create new material</source>
        <translation>创建新的材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="122"/>
        <source>Create new multi-material</source>
        <translation>创建新的复合材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="130"/>
        <source>Merge duplicates</source>
        <translation>合并重复</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="136"/>
        <source>Delete unused</source>
        <translation>删除未使用的</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="152"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="154"/>
        <source>Duplicate</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="156"/>
        <source>Merge to...</source>
        <translation>合并到...</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="158"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="210"/>
        <source>Merging duplicate material</source>
        <translation>合并重复材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="216"/>
        <source>Unable to delete material</source>
        <translation>无法删除材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="216"/>
        <source>InList not empty</source>
        <translation>InList 不为空</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="238"/>
        <source>Deleting unused material</source>
        <translation>删除未使用的材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="288"/>
        <source>Select material to merge to</source>
        <translation>选择要合并的材料</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="322"/>
        <source>This material is used by:</source>
        <translation>本材料使用于：</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="112"/>
        <source>Add property...</source>
        <translation>添加属性...</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="113"/>
        <source>Add property set...</source>
        <translation>添加属性集...</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="113"/>
        <source>New...</source>
        <translation>新建...</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="144"/>
        <source>IFC type</source>
        <translation>IFC 类型</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="144"/>
        <source>Search results</source>
        <translation>搜索结果</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="513"/>
        <source>Please select or create a property set first in which the new property should be placed.</source>
        <translation>请首先选择或创建一个应在其中放置新属性的属性集。</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="524"/>
        <source>New property set</source>
        <translation>新属性集</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="524"/>
        <source>Property set name:</source>
        <translation>属性集名称：</translation>
    </message>
    <message>
        <location filename="../BimIfcElements.py" line="126"/>
        <source>Material</source>
        <translation>材质</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="60"/>
        <source>The Parts Library could not be found.</source>
        <translation>找不到零件库。</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="205"/>
        <source>Error: Unable to import SAT files - CadExchanger addon must be installed</source>
        <translation>错误：无法导入 SAT 文件 - 必须安装 CadExchanger 插件</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="236"/>
        <source>Insertion point</source>
        <translation>插入点</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Origin</source>
        <translation>原点</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Top left</source>
        <translation>左上</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Top center</source>
        <translation>顶部中心</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Top right</source>
        <translation>右上</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Middle left</source>
        <translation>左中</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Middle center</source>
        <translation>正中</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Middle right</source>
        <translation>右中</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Bottom left</source>
        <translation>左下</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Bottom center</source>
        <translation>底部居中</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="243"/>
        <source>Bottom right</source>
        <translation>右下</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="100"/>
        <source>Passed</source>
        <translation>通过</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="101"/>
        <source>This test has succeeded.</source>
        <translation>此测试成功。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="110"/>
        <source>This test has failed. Press the button to know more</source>
        <translation>此测试失败。按下按钮了解更多</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="118"/>
        <source>Test</source>
        <translation>测试</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="119"/>
        <source>Press to perform the test</source>
        <translation>点击执行测试</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="222"/>
        <source>ifcopenshell is not installed on your system or not available to FreeCAD. This library is responsible for IFC support in FreeCAD, and therefore IFC support is currently disabled. Check https://www.freecadweb.org/wiki/Extra_python_modules#IfcOpenShell to obtain more information.</source>
        <translation>ifcopenshell 尚未安装在您的系统上或对 FreeCAD 不可用。 该库负责 FreeCAD 中的 IFC 支持，因此当前禁用了 IFC 支持。 检查 https://www.freecadweb.org/wiki/Extra_python_modules#IfcOpenShell 以获取更多信息。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="229"/>
        <source>The version of ifcopenshell installed on your system will produce files with this schema version:</source>
        <translation>安装在系统上的 ifcopenshell 版本将生成具有以下架构版本的文件：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="260"/>
        <source>The following types were not found in the project:</source>
        <translation>项目中没有找到下列类型：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="301"/>
        <source>The following Building objects have been found to not be included in any Site. You can resolve the situation by creating a Site object, if none is present in your model, and drag and drop the Building objects into it in the tree view:</source>
        <translation>发现以下建筑对象未包含在任何场地中。 如果模型中不存在场地对象，则可以通过创建场地对象来解决这种情况，然后将 Treeing 对象拖放到树视图中：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="337"/>
        <source>The following Building Storey (BuildingParts with their IFC role set as &quot;Building Storey&quot;) objects have been found to not be included in any Building. You can resolve the situation by creating a Building object, if none is present in your model, and drag and drop the Building Storey objects into it in the tree view:</source>
        <translation>已发现以下建筑物楼层（将 IFC 角色设置为“建筑物楼层”的 BuildingingParts）对象不包含在任何建筑物中。 如果模型中不存在建筑对象，则可以通过创建建筑对象来解决这种情况，然后在树视图中将建筑层对象拖放到其中：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="372"/>
        <source>The following BIM objects have been found to not be included in any Building Storey (BuildingParts with their IFC role set as &quot;Building Storey&quot;). You can resolve the situation by creating a Building Storey object, if none is present in your model, and drag and drop these objects into it in the tree view:</source>
        <translation>已发现以下 BIM 对象不包含在任何建筑物楼层中（其 IFC 角色设置为“建筑物楼层”的 BuildingParts）。 如果模型中不存在建筑物，则可以通过创建建筑物对象来解决这种情况，然后将这些对象拖放到树视图中：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="413"/>
        <source>The following BIM objects have the &quot;Undefined&quot; type:</source>
        <translation>以下 BIM 对象具有“未定义”类型：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="417"/>
        <source>The following objects are not BIM objects:</source>
        <translation>以下对象不是 BIM 对象：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="420"/>
        <source>You can turn these objects into BIM objects by using the Utils -&gt; Make Component tool.</source>
        <translation>您可以使用实用程序 -&gt; 制作组件工具将这些对象转换为 BIM 对象。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="449"/>
        <source>The following BIM objects have an invalid or non-solid geometry:</source>
        <translation>以下 BIM 对象具有无效或非实体几何：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="483"/>
        <source>The objects below have Length, Width or Height properties, but these properties won&apos;t be explicitely exported to IFC. This is not necessarily an issue, unless you specifically want these quantities to be exported:</source>
        <translation>以下对象具有“长度”，“宽度”或“高度”属性，但是这些属性不会明确导出到 IFC。 除非您特别希望导出以下数量，否则这不一定是问题：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="486"/>
        <source>To enable exporting of these quantities, use the IFC quantities manager tool located under menu Manage -&gt; Manage IFC Quantities...</source>
        <translation>要启用这些数量的导出，请使用菜单“管理”-&gt;“管理 IFC 数量...”下的 IFC 数量管理器工具。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="537"/>
        <source>The objects below have a defined IFC type but do not have the associated common property set:</source>
        <translation>以下对象具有已定义的 IFC 类型，但没有关联的通用属性集：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="540"/>
        <source>To add common property sets to these objects, use the IFC properties manager tool located under menu Manage -&gt; Manage IFC Properties...</source>
        <translation>要将通用属性集添加到这些对象，请使用菜单“管理”-&gt;“管理 IFC 属性...”下的 IFC 属性管理器工具。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="601"/>
        <source>The objects below have a common property set but that property set doesn&apos;t contain all the needed properties:</source>
        <translation>以下对象具有一个公共属性集，但该属性集未包含所有必需的属性：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="604"/>
        <source>Verify which properties a certain property set must contain on http://www.buildingsmart-tech.org/ifc/IFC4/Add2/html/annex/annex-b/alphabeticalorder_psets.htm</source>
        <translation>在 http://www.buildingsmart-tech.org/ifc/IFC4/Add2/html/annex/annex-b/alphabeticalorder_psets.htm 上验证某个属性集必须包含哪些属性</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="605"/>
        <source>To fix the property sets of these objects, use the IFC properties manager tool located under menu Manage -&gt; Manage IFC Properties...</source>
        <translation>要修复这些对象的属性集，请使用菜单“管理”-&gt;“管理 IFC 属性...”下的 IFC 属性管理器工具。</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="633"/>
        <source>The following BIM objects have no material attributed:</source>
        <translation>以下 BIM 对象没有材质属性：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="668"/>
        <source>The following BIM objects have no defined standard code:</source>
        <translation>以下 BIM 对象没有定义的标准代码：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="710"/>
        <source>The following BIM objects are not extrusions:</source>
        <translation>以下 BIM 对象不是拉伸：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="743"/>
        <source>The following BIM objects are not standard cases:</source>
        <translation>以下 BIM 对象不是标准情况：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="784"/>
        <source>The objects below have lines smaller than 1/32 inch or 0.79 mm, which is the smallest line size that Revit accepts. These objects will be discarded when imported into Revit:</source>
        <translation>下面的对象的线条小于 1/32 英寸或 0.79 毫米，这是 Revit 接受的最小线条尺寸。 将这些对象导入 Revit 后将被丢弃：</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="787"/>
        <source>An additional object, called &quot;TinyLinesResult&quot; has been added to this model, and selected. It contains all the tiny lines found, so you can inspect them and fix the needed objects. Be sure to delete the TinyLinesResult object when you are done!</source>
        <translation>名为 &quot;TinyLinesResult&quot; 的附加对象已添加到此模型，并已选择。 它包含找到的所有细线，因此您可以检查它们并修复所需的对象。 完成后，请确保删除 TinyLinesResult 对象！</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="788"/>
        <source>Tip: The results are best viewed in Wireframe mode (menu Views -&gt; Draw Style -&gt; Wireframe)</source>
        <translation>提示：最好在线框模式下查看结果（菜单视图-&gt;绘制样式-&gt;线框）</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="78"/>
        <source>No active document, aborting.</source>
        <translation>没有活动的文档，正在中止。</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="115"/>
        <source>Building Outline</source>
        <translation>建筑物轮廓</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="120"/>
        <source>Building Layout</source>
        <translation>建筑物布局</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="125"/>
        <source>Building Label</source>
        <translation>建筑物标签</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="133"/>
        <source>Vertical Axes</source>
        <translation>垂直轴</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="146"/>
        <source>Horizontal Axes</source>
        <translation>水平轴</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="160"/>
        <source>Axes</source>
        <translation>轴线</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="172"/>
        <source>Level</source>
        <translation>水平</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="184"/>
        <source>Level Axes</source>
        <translation>水平轴</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="213"/>
        <source>New Group</source>
        <translation>新建组</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="227"/>
        <source>Save preset</source>
        <translation>保存预设</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="227"/>
        <source>Preset name:</source>
        <translation>预设名称：</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="278"/>
        <source>User preset...</source>
        <translation>用户预设...</translation>
    </message>
    <message>
        <location filename="../BimReextrude.py" line="143"/>
        <source>Error: Please select exactly one base face</source>
        <translation>错误：请选择一个基准面</translation>
    </message>
    <message>
        <location filename="../BimSetup.py" line="92"/>
        <source>Tip: Some additional workbenches are not installed, that extend BIM functionality:</source>
        <translation>提示：未安装一些扩展 BIM 功能的其他工作台：</translation>
    </message>
    <message>
        <location filename="../BimSetup.py" line="92"/>
        <source>You can install them from menu Tools -&gt; Addon manager.</source>
        <translation>您可以从菜单工具 -&gt; 插件管理器安装它们。</translation>
    </message>
    <message>
        <location filename="../BimTutorial.py" line="123"/>
        <source>Downloading images...</source>
        <translation>正在下载图像...</translation>
    </message>
    <message>
        <location filename="../BimTutorial.py" line="195"/>
        <source>BIM Tutorial - step</source>
        <translation>BIM 教程 - 步骤</translation>
    </message>
    <message>
        <location filename="../BimUnclone.py" line="113"/>
        <source>Draft Clones are not supported yet!</source>
        <translation>目前不支持底图复制！</translation>
    </message>
    <message>
        <location filename="../BimUnclone.py" line="115"/>
        <source>The selected object is not a clone</source>
        <translation>所选对象不是副本</translation>
    </message>
    <message>
        <location filename="../BimUnclone.py" line="117"/>
        <source>Please select exactly one object</source>
        <translation>请仅选择一个对象</translation>
    </message>
    <message>
        <location filename="../BimWindows.py" line="223"/>
        <source>None</source>
        <translation>无</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="209"/>
        <source>&amp;2D Drafting</source>
        <translation>&amp;2D 制图</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="210"/>
        <source>&amp;3D/BIM</source>
        <translation>&amp;3D/BIM</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="211"/>
        <source>&amp;Annotation</source>
        <translation>注释(&amp;A)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="212"/>
        <source>&amp;Snapping</source>
        <translation>捕捉(&amp;S)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="213"/>
        <source>&amp;Modify</source>
        <translation>修改(&amp;M)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="214"/>
        <source>&amp;Manage</source>
        <translation>管理(&amp;M)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="216"/>
        <source>&amp;Flamingo</source>
        <translation>火烈鸟(&amp;F)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="218"/>
        <source>&amp;Fasteners</source>
        <translation>紧固件(&amp;F)</translation>
    </message>
    <message>
        <location filename="../InitGui.py" line="220"/>
        <source>&amp;Utils</source>
        <translation>工具(&amp;U)</translation>
    </message>
</context>
<context>
    <name>BIM_Beam</name>
    <message>
        <location filename="../BimCommands.py" line="427"/>
        <source>Beam</source>
        <translation>光束</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="428"/>
        <source>Creates a beam between two points</source>
        <translation>在两点之间创建梁</translation>
    </message>
</context>
<context>
    <name>BIM_Box</name>
    <message>
        <location filename="../BimBox.py" line="41"/>
        <source>Box</source>
        <translation>立方体</translation>
    </message>
    <message>
        <location filename="../BimBox.py" line="42"/>
        <source>Graphically creates a generic box in the current document</source>
        <translation>在当前文档中以图形方式创建通用框</translation>
    </message>
</context>
<context>
    <name>BIM_Classification</name>
    <message>
        <location filename="../BimClassification.py" line="41"/>
        <source>Manage classification...</source>
        <translation>管理分类...</translation>
    </message>
    <message>
        <location filename="../BimClassification.py" line="42"/>
        <source>Manage how the different materials of this documents use classification systems</source>
        <translation>管理本文档的不同材料如何使用分类系统</translation>
    </message>
</context>
<context>
    <name>BIM_Column</name>
    <message>
        <location filename="../BimCommands.py" line="412"/>
        <source>Column</source>
        <translation>圆柱</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="413"/>
        <source>Creates a column at a specified location</source>
        <translation>在指定位置创建圆柱</translation>
    </message>
</context>
<context>
    <name>BIM_Convert</name>
    <message>
        <location filename="../BimCommands.py" line="314"/>
        <source>Convert to BIM type...</source>
        <translation>转换为 BIM 类型...</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="315"/>
        <source>Converts any object to a BIM component</source>
        <translation>将任何对象转换为 BIM 组件</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="376"/>
        <source>Remove from group</source>
        <translation>从组中删除</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="377"/>
        <source>Removes this object from its parent group</source>
        <translation>从父组移除该对象</translation>
    </message>
</context>
<context>
    <name>BIM_Copy</name>
    <message>
        <location filename="../BimCommands.py" line="161"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
</context>
<context>
    <name>BIM_Diff</name>
    <message>
        <location filename="../BimDiff.py" line="38"/>
        <source>IFC Diff</source>
        <translation>IFC 差异</translation>
    </message>
    <message>
        <location filename="../BimDiff.py" line="39"/>
        <source>Shows the difference between two IFC-based documents</source>
        <translation>显示两个基于 IFC 的文档之间的差别</translation>
    </message>
</context>
<context>
    <name>BIM_Glue</name>
    <message>
        <location filename="../BimCommands.py" line="198"/>
        <source>Glue</source>
        <translation>粘合</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="199"/>
        <source>Joins selected shapes into one non-parametric shape</source>
        <translation>将选定的形状连接为一个非参数形状</translation>
    </message>
</context>
<context>
    <name>BIM_Help</name>
    <message>
        <location filename="../BimCommands.py" line="182"/>
        <source>BIM Help</source>
        <translation>BIM 帮助</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="183"/>
        <source>Opens the BIM help page on the FreeCAD documentation website</source>
        <translation>打开 FreeCAD 文档网站上的 BIM 帮助页面</translation>
    </message>
</context>
<context>
    <name>BIM_IfcElements</name>
    <message>
        <location filename="../BimIfcElements.py" line="40"/>
        <source>Manage IFC elements...</source>
        <translation>管理 IFC 元素...</translation>
    </message>
    <message>
        <location filename="../BimIfcElements.py" line="41"/>
        <source>Manage how the different elements of of your BIM project will be exported to IFC</source>
        <translation>管理如何将 BIM 项目的不同元素导出到 IFC</translation>
    </message>
</context>
<context>
    <name>BIM_IfcProperties</name>
    <message>
        <location filename="../BimIfcProperties.py" line="41"/>
        <source>Manage IFC properties...</source>
        <translation>管理 IFC 属性...</translation>
    </message>
    <message>
        <location filename="../BimIfcProperties.py" line="42"/>
        <source>Manage the different IFC properties of your BIM objects</source>
        <translation>管理 BIM 对象的不同 IFC 属性</translation>
    </message>
</context>
<context>
    <name>BIM_IfcQuantities</name>
    <message>
        <location filename="../BimIfcQuantities.py" line="49"/>
        <source>Manage IFC quantities...</source>
        <translation>管理 IFC 数量...</translation>
    </message>
    <message>
        <location filename="../BimIfcQuantities.py" line="50"/>
        <source>Manage how the quantities of different elements of of your BIM project will be exported to IFC</source>
        <translation>管理如何将 BIM 项目中不同元素的数量导出到 IFC</translation>
    </message>
</context>
<context>
    <name>BIM_Levels</name>
    <message>
        <location filename="../BimLevels.py" line="38"/>
        <source>Manage levels...</source>
        <translation>管理水平...</translation>
    </message>
    <message>
        <location filename="../BimLevels.py" line="39"/>
        <source>Set/modify the different levels of your BIM project</source>
        <translation>设置/修改 BIM 项目的不同水平</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="37"/>
        <source>Manage project...</source>
        <translation>管理项目...</translation>
    </message>
    <message>
        <location filename="../BimProject.py" line="38"/>
        <source>Setup your BIM project</source>
        <translation>设置您的 BIM 项目</translation>
    </message>
    <message>
        <location filename="../BimWindows.py" line="39"/>
        <source>Manage the different doors and windows of your BIM project</source>
        <translation>管理 BIM 项目的不同门窗</translation>
    </message>
</context>
<context>
    <name>BIM_Library</name>
    <message>
        <location filename="../BimLibrary.py" line="41"/>
        <source>Objects library</source>
        <translation>对象库</translation>
    </message>
    <message>
        <location filename="../BimLibrary.py" line="42"/>
        <source>Opens the objects library</source>
        <translation>打开对象库</translation>
    </message>
</context>
<context>
    <name>BIM_Material</name>
    <message>
        <location filename="../BimMaterial.py" line="60"/>
        <source>Material</source>
        <translation>材质</translation>
    </message>
    <message>
        <location filename="../BimMaterial.py" line="62"/>
        <source>Sets or creates a material for selected objects</source>
        <translation>设置或创建所选对象的材料</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Down</name>
    <message>
        <location filename="../BimNudge.py" line="160"/>
        <source>Nudge Down</source>
        <translation>向下微调</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Extend</name>
    <message>
        <location filename="../BimNudge.py" line="211"/>
        <source>Nudge Extend</source>
        <translation>微调扩展</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Left</name>
    <message>
        <location filename="../BimNudge.py" line="177"/>
        <source>Nudge Left</source>
        <translation>向左微调</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Right</name>
    <message>
        <location filename="../BimNudge.py" line="194"/>
        <source>Nudge Right</source>
        <translation>向右微调</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_RotateLeft</name>
    <message>
        <location filename="../BimNudge.py" line="247"/>
        <source>Nudge Rotate Left</source>
        <translation>向左微调旋转</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_RotateRight</name>
    <message>
        <location filename="../BimNudge.py" line="264"/>
        <source>Nudge Rotate Right</source>
        <translation>向右微调旋转</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Shrink</name>
    <message>
        <location filename="../BimNudge.py" line="229"/>
        <source>Nudge Shrink</source>
        <translation>微调收缩</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Switch</name>
    <message>
        <location filename="../BimNudge.py" line="120"/>
        <source>Nudge Switch</source>
        <translation>微调切换</translation>
    </message>
</context>
<context>
    <name>BIM_Nudge_Up</name>
    <message>
        <location filename="../BimNudge.py" line="143"/>
        <source>Nudge Up</source>
        <translation>向上微调</translation>
    </message>
</context>
<context>
    <name>BIM_Preflight</name>
    <message>
        <location filename="../BimPreflight.py" line="56"/>
        <source>Preflight checks...</source>
        <translation>预检查...</translation>
    </message>
    <message>
        <location filename="../BimPreflight.py" line="57"/>
        <source>Checks several characteristics of this model before exporting to IFC</source>
        <translation>在导出到 IFC 之前检查此模型的几个特征</translation>
    </message>
</context>
<context>
    <name>BIM_Reextrude</name>
    <message>
        <location filename="../BimReextrude.py" line="40"/>
        <source>Reextrude</source>
        <translation>重新拉伸</translation>
    </message>
    <message>
        <location filename="../BimReextrude.py" line="41"/>
        <source>Recreates an extruded Structure from a selected face</source>
        <translation>从选定面重新创建拉伸结构</translation>
    </message>
</context>
<context>
    <name>BIM_Setup</name>
    <message>
        <location filename="../BimSetup.py" line="39"/>
        <source>BIM Setup...</source>
        <translation>BIM 设置...</translation>
    </message>
    <message>
        <location filename="../BimSetup.py" line="40"/>
        <source>Set some common FreeCAD preferences for BIM workflow</source>
        <translation>为 BIM 工作流程设置一些常见的 FreeCAD 首选项</translation>
    </message>
</context>
<context>
    <name>BIM_Sketch</name>
    <message>
        <location filename="../BimCommands.py" line="236"/>
        <source>Sketch</source>
        <translation>草图</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="237"/>
        <source>Creates a new sketch in the current working plane</source>
        <translation>在当前工作平面中创建一个新草图</translation>
    </message>
</context>
<context>
    <name>BIM_TogglePanels</name>
    <message>
        <location filename="../BimCommands.py" line="48"/>
        <source>Toggle panels</source>
        <translation>切换面板</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="49"/>
        <source>Toggle report panels on/off</source>
        <translation>打开/关闭报告面板</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="82"/>
        <source>Move to Trash</source>
        <translation>移动到回收站</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="83"/>
        <source>Moves the selected objects to the Trash folder</source>
        <translation>将所选对象移动到“回收站”文件夹</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="122"/>
        <source>Clean Trash</source>
        <translation>清空回收站</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="123"/>
        <source>Deletes from the trash bin all objects that are not used by any other</source>
        <translation>从回收站中删除所有其他未使用的对象</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="162"/>
        <source>Copies selected objects to another location</source>
        <translation>将所选对象复制到另一个位置</translation>
    </message>
    <message>
        <location filename="../BimCurtainWall.py" line="40"/>
        <source>Curtain wall</source>
        <translation>幕墙</translation>
    </message>
    <message>
        <location filename="../BimCurtainWall.py" line="41"/>
        <source>Builds a curtain wall from a selected face</source>
        <translation>从选定面构建幕墙</translation>
    </message>
</context>
<context>
    <name>BIM_Tutorial</name>
    <message>
        <location filename="../BimTutorial.py" line="54"/>
        <source>BIM Tutorial</source>
        <translation>BIM 教程</translation>
    </message>
    <message>
        <location filename="../BimTutorial.py" line="55"/>
        <source>Starts or continues the BIM in-game tutorial</source>
        <translation>开始或继续 BIM 游戏内教程</translation>
    </message>
</context>
<context>
    <name>BIM_Unclone</name>
    <message>
        <location filename="../BimUnclone.py" line="40"/>
        <source>Unclone</source>
        <translation>取消克隆</translation>
    </message>
    <message>
        <location filename="../BimUnclone.py" line="41"/>
        <source>Makes a selected clone object independent from its original</source>
        <translation>使选定的克隆对象独立于其原始对象</translation>
    </message>
</context>
<context>
    <name>BIM_Views</name>
    <message>
        <location filename="../BimViews.py" line="37"/>
        <source>Views manager</source>
        <translation>视图管理器</translation>
    </message>
    <message>
        <location filename="../BimViews.py" line="38"/>
        <source>Shows or hides the views manager</source>
        <translation>显示或隐藏视图管理器</translation>
    </message>
</context>
<context>
    <name>BIM_WPView</name>
    <message>
        <location filename="../BimCommands.py" line="269"/>
        <source>Working Plane View</source>
        <translation>工作平面视图</translation>
    </message>
    <message>
        <location filename="../BimCommands.py" line="270"/>
        <source>Aligns the view on the current item in BIM Views window or on the current working plane</source>
        <translation>在 BIM 视图窗口中或在当前工作平面上对齐当前项上的视图</translation>
    </message>
</context>
<context>
    <name>BIM_Welcome</name>
    <message>
        <location filename="../BimWelcome.py" line="39"/>
        <source>BIM Welcome screen</source>
        <translation>BIM 欢迎界面</translation>
    </message>
    <message>
        <location filename="../BimWelcome.py" line="40"/>
        <source>Show the BIM workbench welcome screen</source>
        <translation>显示 BIM 工作台欢迎屏幕</translation>
    </message>
</context>
<context>
    <name>BIM_Windows</name>
    <message>
        <location filename="../BimWindows.py" line="38"/>
        <source>Manage doors and windows...</source>
        <translation>管理门窗...</translation>
    </message>
</context>
</TS>
