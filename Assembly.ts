<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en">
<context>
    <name>AssemblyGui::TaskAssemblyConstraints</name>
    <message>
        <location filename="../../TaskAssemblyConstraints.cpp" line="53"/>
        <source>Constraints</source>
        <translation>约束</translation>
    </message>
</context>
<context>
    <name>AssemblyGui::Workbench</name>
    <message>
        <location filename="../../Workbench.cpp" line="94"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyAddExistingComponent</name>
    <message>
        <location filename="../../Command.cpp" line="154"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="155"/>
        <source>Add existing Component...</source>
        <translation>添加现有组件...</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="156"/>
        <source>Add a existing Component into the active Assembly, STEP, IGES or BREP</source>
        <translation>将现有零部件添加到活动的装配，STEP，IGES 或 BREP 中</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyAddNewComponent</name>
    <message>
        <location filename="../../Command.cpp" line="108"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="109"/>
        <source>Add new Assembly</source>
        <translation>添加新装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="110"/>
        <source>Add a new Subassembly into the active Assembly</source>
        <translation>将新的装配添加到活动装配中</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyAddNewPart</name>
    <message>
        <location filename="../../Command.cpp" line="52"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="53"/>
        <source>Add new Part</source>
        <translation>添加新零件</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="54"/>
        <source>Add a new Part into the active Assembly</source>
        <translation>向活动装配中添加新零件</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraint</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="118"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="119"/>
        <source>Constraint</source>
        <translation>约束</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="120"/>
        <source>Add arbitrary constraints to the assembly</source>
        <translation>向部件添加任意约束</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintAlignment</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="539"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="540"/>
        <source>Constraint alignment...</source>
        <translation>约束对齐...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="541"/>
        <source>Align the selected entities</source>
        <translation>对齐所选实体</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintAngle</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="334"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="335"/>
        <source>Constraint Angle...</source>
        <translation>约束角度...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="336"/>
        <source>Set the angle between two selected entities</source>
        <translation>设置两个选定图元之间的角度</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintCoincidence</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="471"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="472"/>
        <source>Constraint coincidence...</source>
        <translation>约束重合...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="473"/>
        <source>Make the selected entities coincident</source>
        <translation>使选定图元重合</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintDistance</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="202"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="203"/>
        <source>Constraint Distance...</source>
        <translation>约束距离...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="204"/>
        <source>Set the distance between two selected entities</source>
        <translation>设置两个选定实体之间的距离</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintFix</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="269"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="270"/>
        <source>Constraint Fix...</source>
        <translation>约束修复...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="271"/>
        <source>Fix a part in it&apos;s rotation and translation</source>
        <translation>将零件固定在其旋转和平移中</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyConstraintOrientation</name>
    <message>
        <location filename="../../CommandConstraints.cpp" line="403"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="404"/>
        <source>Constraint Orientation...</source>
        <translation>约束方向...</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="405"/>
        <source>Set the orientation of two selected entities in regard to each other</source>
        <translation>设置两个选定实体相对于彼此的方向</translation>
    </message>
</context>
<context>
    <name>CmdAssemblyImport</name>
    <message>
        <location filename="../../Command.cpp" line="217"/>
        <source>Assembly</source>
        <translation>装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="218"/>
        <source>Import assembly...</source>
        <translation>导入装配...</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="219"/>
        <source>Import one or more files and create a assembly structure.</source>
        <translation>导入一个或多个文件并创建装配结构。</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../Command.cpp" line="73"/>
        <location filename="../../Command.cpp" line="129"/>
        <location filename="../../Command.cpp" line="175"/>
        <source>No active or selected assembly</source>
        <translation>没有活动或选定的装配</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="74"/>
        <source>You need a active or selected assembly to insert a part in.</source>
        <translation>您需要激活或选定的装配才能在其中插入零件。</translation>
    </message>
    <message>
        <location filename="../../Command.cpp" line="130"/>
        <location filename="../../Command.cpp" line="176"/>
        <source>You need a active or selected assembly to insert a component in.</source>
        <translation>您需要激活或选定的装配才能在其中插入零部件。</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="70"/>
        <source>No active Assembly</source>
        <translation>没有活动的装配</translation>
    </message>
    <message>
        <location filename="../../CommandConstraints.cpp" line="71"/>
        <source>You need a active (blue) Assembly to insert a Constraint. Please create a new one or make one active (double click).</source>
        <translation>您需要激活(蓝色)装配才能插入约束。请新建一个或激活一个(双击)。</translation>
    </message>
</context>
<context>
    <name>TaskAssemblyConstraints</name>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="14"/>
        <source>Form</source>
        <translation>窗体</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="22"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The first geometry to which the constraint relates. Note that first and second geometry can be swapt. If you want to clear it, use the button to the right. If it is empty, just select any geometry in the 3D view and it will be added here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;与约束相关的第一个几何图元。请注意，第一个和第二个几何图形可以互换。如果您想清除它，请使用右边的按钮。如果为空，只需选择3D视图中的任何几何图形，即可将其添加到此处。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="35"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear the first geometry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;清除第一个几何图形&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="38"/>
        <location filename="../../TaskAssemblyConstraints.ui" line="72"/>
        <location filename="../../TaskAssemblyConstraints.ui" line="439"/>
        <location filename="../../TaskAssemblyConstraints.ui" line="477"/>
        <location filename="../../TaskAssemblyConstraints.ui" line="512"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="56"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;The second geometry to which the constraint relates. Note that first and second geometry can be swapt. If you want to clear it, use the button to the right. If it is empty, just select any geometry in the 3D view and it will be added here.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;与约束相关的第二个几何图元。请注意，第一个和第二个几何图形可以互换。如果您想清除它，请使用右边的按钮。如果它是空的，只需选择 3D 视图中的任何几何图形，它就会添加到这里。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="69"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Clear the second geometry&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;清除第二个几何图形&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="113"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the angle between the geometries normals&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;设置几何图形法线之间的角度&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="116"/>
        <source>Angle</source>
        <translation>角度</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="154"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Special constraint which is in general used to let the geometries be on each other. Therefore it&apos;s often the same as align, with the difference that it is also defined for points, as a point can lie on a plane. Note that this constraint has a special behaviour for cylinders. For example, a cylindrical surface can&apos;t be on a plane, only touch it. Therefore this is not valid. Furthermore point and line coincident with cylinders don&apos;t work on the cylinder surface, but on its center line. The reason for that it is, that this centerline would not be accessible with other constraints, but the surface coincident can be also achieved with the align constraint and value 0.  At last specialty the cylinder cylinder constraint shall be mentioned: It works also on the cylinder centerlines and therefore makes them concentric. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;特殊约束，通常用于使几何图形相互连接。因此，它通常与对齐相同，不同之处在于它也是为点定义的，因为点可以位于平面上。请注意，此约束对于圆柱体具有特殊行为。例如，柱面不能在平面上，只能接触它。因此，这是无效的。此外，与圆柱体重合的点和线不在圆柱面上工作，而是在圆柱体的中心线上工作。原因是，使用其他约束时无法访问此中心线，但是也可以使用对齐约束和值0来实现曲面重合。最后要提到圆柱圆柱约束：它也作用于圆柱中心线，因此使它们同心。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="157"/>
        <source>Coincident</source>
        <translation>约束</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="195"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Fixes the first geometry in its rotation and translation. Note that fix only works its the direct parent assembly. If you stack assemblys, the parent assembly will not be fixed inside the other ones.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;修复旋转和平移中的第一个几何体。请注意，修复仅适用于其直接父级装配。如果堆叠装配，父级装配将不会固定在其他装配内。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="198"/>
        <source>Fix</source>
        <translation>修正</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="236"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Set the distance between first and second geometry. Note that in many cases the shortest distance is used (e.g. line - line)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;设置第一个几何图形和第二个几何图形之间的距离。请注意，在许多情况下，使用最短距离(例如，直线-直线)&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="239"/>
        <source>Distance</source>
        <translation>距离</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="277"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Allows to set the orientation of the geometries normals in relation to each other. Possible values are parallel (means equal or opposite normals), equal normals, opposite normals or perpendicular ones. Note that for cylinders the base circles normal is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;允许设置几何体法线相对于彼此的方向。可能的值有平行(表示法线相等或相反)、法线相等、法线相反或垂直。请注意，对于圆柱体，使用基圆法线。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="280"/>
        <source>Orientation</source>
        <translation>方向</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="318"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Adds a orientation and a distance constraint. Therefore this constraint is only valid where both of the individual constraints are, e.g. you can&apos;t align a point and a plane as point-plane orientation is invalid. Furthermore it can happen that this constraint is only valid for a certain orientation, e.g. plane - line has only a defined distance, when the orientation is perpendicular. The reason behind this is, that a non-perpendicular line would always cut the plane and therefore the shortest distance would always be 0. &lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;添加方向和距离约束。因此，此约束仅在两个单独约束都存在的情况下才有效，例如，您不能对齐点和平面，因为点-平面方向无效。此外，当方向垂直时，该约束可能仅对特定方向有效，例如，平面线仅具有定义的距离。这背后的原因是，非垂直线始终与平面相交，因此最短距离始终为 0。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="321"/>
        <source>Align</source>
        <translation>对齐</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="413"/>
        <source>value</source>
        <translation>值</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="436"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Use the full solution space. The nearest solution will be found.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使用完整的解决方案空间。将会找到最近的解决方案。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="474"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Positive solution space. Reduces the valid solutions to the positive domain, e.g. point over the plane at specified distance, not under. Or point outside a cylinder at specified distance, not inside.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;正解空间。将有效解减少到正域，例如指定距离的平面上方的点，而不是下方的点。或指向圆柱体外部的指定距离，而不是内部。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="509"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Negative solution space. Reduces the valid solutions to the negative domain, e.g. point under the plane at specified distance, not over. Or point inside a cylinder at specified distance, not outside.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;负解空间。将有效解减少到负域，例如，平面下指定距离处的点，而不是超过该距离的点。或圆柱体内部指定距离的点，而不是圆柱体外部的点。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="566"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes the geometries normals parallel, that means they can point in the same or opposite direction. Note that for cylinders the base circles normal is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使几何体法线平行，这意味着它们可以指向相同或相反的方向。请注意，对于圆柱体，使用基圆法线。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="569"/>
        <source>Parallel</source>
        <translation>平行</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="604"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes the geometries normals point in the same direction. Note that for cylinders the base circles normal is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使几何体法线指向同一方向。请注意，对于圆柱体，使用基圆法线。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="607"/>
        <source>Equal</source>
        <translation>相等</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="645"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes the geometries normals point in the opposite direction. Note that for cylinders the base circles normal is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使几何法线指向相反的方向。 注意，对于圆柱体，使用法线基圆。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="648"/>
        <source>Opposite</source>
        <translation>相对</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="683"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;Makes the geometries normals perpendicular. Note that for cylinders the base circles normal is used.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;使几何体法线垂直。 注意，对于圆柱体，使用法线基圆。&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="../../TaskAssemblyConstraints.ui" line="689"/>
        <source>Perpend.</source>
        <translation>垂直。</translation>
    </message>
</context>
</TS>
