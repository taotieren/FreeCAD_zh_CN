# FreeCAD_zh_CN

#### 介绍
FreeCAD 简体中文翻译。为了解决群有无法正常使用 Crowdin 翻译而建立。仓库更新会同步到官方仓库上。

### 官方仓库地址

[FreeCAD](https://crowdin.com/project/freecad) 

``` bash
https://crowdin.com/project/freecad
```


#### 安装教程

1.  克隆仓库
``` bash
git clone https://gitee.com/taotieren/FreeCAD_zh_CN.git
```

2.  参与翻译

2.1  Poedit 编辑 `*.po` 文件保存后会自动生存编译好的 `*.mo` 文件
``` bash
poedit *.po
```

2.2  QT Linguist （QT 语言家）编辑 `*.ts` 文件

``` bash
# Windows 用户自行从 QT 官网或者 TUNA 镜像站点下载 qt-creator 安装后会自带 QT 语言家。
# Linux 用户通过自己所用发行版的仓库安装即可。包名：qt-creator

```


3.  翻译排版参考手册

``` bash
中文简体版拍指南地址：https://github.com/taotieren/chinese-copywriting-guidelines
码云镜像地址：https://gitee.com/KiCAD-CN/chinese-copywriting-guidelines

```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
